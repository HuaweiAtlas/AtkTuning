import torch
import caffe
from torch.autograd import Variable
import numpy as np
from fpn import FPN101


input_size = [1, 3, 328, 400]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

net = FPN101()
checkpoint = torch.load('fpn-pytorch.pth')
net.load_state_dict(checkpoint)
net.eval()
input_var = Variable(torch.from_numpy(input_data))
fms = net.forward(input_var)

model = './fpn.prototxt'
weights = './fpn.caffemodel'
net = caffe.Net(model, weights, caffe.TEST)
net.blobs['blob1'].data[...] = input_data
out = net.forward()

conv34_pytorch = fms[2].data.cpu().numpy().flatten()
conv35_pytorch = fms[1].data.cpu().numpy().flatten()
conv36_pytorch = fms[0].data.cpu().numpy().flatten()

print('******diff******')

diff_conv34 = np.abs(conv34_pytorch - out['conv34'].flatten())
data34 = np.abs(conv34_pytorch)

print('diff_conv34:')
print('diff_max:', diff_conv34.max(), '    ', 'diff_mean:', diff_conv34.mean(), '    ',
      'diff_min:', diff_conv34.min(), '    ', 'diff_median:', np.median(diff_conv34))

diff_conv35 = np.abs(conv35_pytorch - out['conv35'].flatten())
print('diff_conv35:')
print('diff_max:', diff_conv35.max(), '    ', 'diff_mean:', diff_conv35.mean(), '    ',
      'diff_min:', diff_conv35.min(), '    ', 'diff_median:', np.median(diff_conv35))

diff_conv36 = np.abs(conv36_pytorch - out['conv36'].flatten())
print('diff_conv36:')
print('diff_max:', diff_conv36.max(), '    ', 'diff_mean:', diff_conv36.mean(), '    ',
      'diff_min:', diff_conv36.min(), '    ', 'diff_median:', np.median(diff_conv36))
