EN|[CN](README.md)
Converting the FPN Model in PyTorch to a Caffe Model

[TOC]

## Preparations

Download [FPN model source code](https://github.com/kuangliu/pytorch-fpn) and modify the `fpn.py` file.

(1) Modify the `_upsample_add` function.
In pytorch1.1.0, the upsample operator is discarded. In this example, the interpolate operator is used. The modification result is as follows:

``` python
  def _upsample_add(self, x, y):
      '''
      Upsample and add two feature maps.
      '''
      _,_,H,W = y.size()
      return F.interpolate(x, size=(H,W), mode='bilinear',align_corners=True) + y
```

(2) Copy the Caffe and convert folders and the pytorch2caffe-fpn.py and test-fpn.py files to the same directory.
Modify layer_param.py in the Caffe folder. 
```python
	def pool_param(self,type='MAX',kernel_size=2,stride=2,pad=None,ceil_mode=False):
	
        ...

        if not ceil_mode:
            pool_param.ceil_mode=ceil_mode
        if pad:
            pool_param.pad=pad
        self.param.pooling_param.CopyFrom(pool_param)
```

## Conversion

```python
python pytorch2caffe-fpn.py
```

After this command is executed, files such as fpn.caffemodel are generated.

## Result Verification

```python
python test-fpn.py
```
