import torch
from torch.autograd import Variable
from convert import pytorch_to_caffe
from fpn import FPN101

net = FPN101()
net.eval()

print('start')
input_var = Variable(torch.randn(1, 3, 328, 400))

pytorch_to_caffe.trans_net(net, [input_var], 'fpn')
pytorch_to_caffe.save_prototxt('./fpn.prototxt')
pytorch_to_caffe.save_caffemodel('./fpn.caffemodel')
torch.save(net.state_dict(), './fpn-pytorch.pth')

print('end')
