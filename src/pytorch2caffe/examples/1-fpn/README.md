[EN](README.en.md)|CN
将pytorch框架下的FPN网络转换成caffe模型

[TOC]

## 准备工作

下载[FPN模型源码](https://github.com/kuangliu/pytorch-fpn)，并对`fpn.py`文件进行修改。

(1) 修改`_upsample_add`函数。
在pytorch1.1.0弃用了upsample算子这里使用interpolate算子代替，修改后结果如下：

``` python
  def _upsample_add(self, x, y):
      '''
      Upsample and add two feature maps.
      '''
      _,_,H,W = y.size()
      return F.interpolate(x, size=(H,W), mode='bilinear',align_corners=True) + y
```

(2) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-fpn.py、test-fpn.py文件到同级目录
修改Caffe文件夹中的layer_param.py
```python
	def pool_param(self,type='MAX',kernel_size=2,stride=2,pad=None,ceil_mode=False):
	
        ...

        if not ceil_mode:
            pool_param.ceil_mode=ceil_mode
        if pad:
            pool_param.pad=pad
        self.param.pooling_param.CopyFrom(pool_param)
```

## 开始转换

```python
python pytorch2caffe-fpn.py
```

执行该命令会生成fpn.caffemodel等文件

## 结果校验

```python
python test-fpn.py
```
