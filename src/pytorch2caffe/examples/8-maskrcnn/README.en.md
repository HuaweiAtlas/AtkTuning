EN|[CN](README.en.md)
Convert MaskRCNN network under pytorch framework to caffe model

[TOC]

## prepare work

(1) Download [MaskRCNN model source code](https://github.com/multimodallearning/pytorch-mask-rcnn),then download the pretrained  [pytorch model](https://drive.google.com/open?id=1LXUgC2IZUYNEoXr05tdqyKFZY0pZyPDc) according to the model source `README`.If you cannot download it, after training the model yourself, place the model in the current directory.

(2)  Compile `nms`, `roialign` source code and configure python interface for coco dataset according to the model source `README`.

(3) Copy the `Caffe`, `convert` folder and `pytorch2caffe-mask-rcnn.py` file in the tool to the same directory.

(4) `F.pad` operator solution

The MaskRCNN model uses the`class SamePad2d(nn.Module):`class during the building process. F.pad operator caffe and D platforms are not supported,There are two ways to solve it.

```python
F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```

Option I：

Delete the application of `SamePad2d ()` in the `model.py` module, and add padding parameters according to the values of F.pad to `nn.Conv2d ()` and `nn.MaxPool2d ()` operators. E.g:

Before modification：

```python
self.P5_conv2 = nn.Sequential(
    SamePad2d(kernel_size=3, stride=1),
    nn.Conv2d(self.out_channels, self.out_channels, kernel_size=3, stride=1),
)
```

After modification：

```python
self.P5_conv2 = nn.Sequential(
    nn.Conv2d(self.out_channels, self.out_channels, kernel_size=3, stride=1,padding=1),
)
```

Option  II：

Convert the F.pad operator to the python layer under the caffe framework. Modify the `mask-rcnn.prototxt` file after the model conversion is complete. Add padding parameters according to the parameter values of the pad operator `nn.Conv2d ()` and `nn.MaxPool2d ()`, and then delete the pad layer.

Parameter description of pad layer:

```python
layer {
  name: "pad6"
  type: "Python"
  bottom: "relu14"
  top: "pad6"
  python_param {
    param_str: "1**1**1**1"
  }
}
```

In `param_str：1 ** 1 ** 1 ** 1` indicates the padding value of the image width and height .

Consistent with

`F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)`.

E.g：

Before modification：

```python
layer {
  name: "pad6"
  type: "Python"
  bottom: "relu14"
  top: "pad6"
  python_param {
    param_str: "1**1**1**1"
  }
}
layer {
  name: "conv17"
  type: "Convolution"
  bottom: "pad6"
  top: "conv17"
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 0
    kernel_size: 3
    
    ...
    
    dilation: 1
  }
}
```

After modification：

```python
layer {
  name: "conv17"
  type: "Convolution"
  bottom: "relu14"
  top: "conv17"
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 1
    kernel_size: 3
    
    ...
    
    dilation: 1
  }
}
```

(5) Modify `pytorch_to_caffe.py` conversion tool

- Comment out the conversion part of the interpolate operator in the `pytorch_to_caffe.py` modulel.

The pytorch 0.4.0 version does not contain the interpolate operator. If it is not commented, an error will be reported.

```python
#F.interpolate = Rp(F.interpolate, _interpolate)
```

- Added support for pad operator conversion in `pytorch_to_caffe.py` module

Due to the uncertainty of the user's setting of the pad parameters in `F.pad ()`, `F.pad ()` 's solution two is temporarily used to convert `F.pad` to the python layer of caffe.

1）Add replacement function in `pytorch_to_caffe.py` module.

```python
def _pad(raw,input,pad,mode,value):
    x=raw(input,pad,mode,value)
    name=log.add_layer(name='pad')
    top_blobs=log.add_blobs([x],name='pad')
    layer=caffe_net.Layer_param(name=name,type='Python',
                                bottom=[log.blobs(input)],top=top_blobs)
    padding_value = str(pad[0])+'**'+str(pad[1])+'**'+str(pad[2])+'**'+str(pad[3]) 
     
    layer.param.python_param.param_str = padding_value
    log.cnet.add_layer(layer)
    return x
```

2) Add replace operation

```python
F.pad=Rp(F.pad,_pad)
```

- Added support for `F.upsample` operator conversion in `pytorch_to_caffe.py` module

1）Add replacement function in `pytorch_to_caffe.py` module.

```python
def _upsample(raw,input,size=None,scale_factor=None,mode='nearest',align_corners=None):
    x = raw(input,size,scale_factor,mode,align_corners)
    name = log.add_layer(name='upsample')
    top_blobs = log.add_blobs([x],name='upsample')
    layer = caffe_net.Layer_param(name=name,type='Upsample',
                                  bottom=[log.blobs(input)],top=top_blobs)
    layer.param.upsample_param.scale=scale_factor
    log.cnet.add_layer(layer)
    return x
```

2)  Add replace operation

```python
F.upsample=Rp(F.upsample,_upsample)
```

- Added support for proposal_layer custom operator conversion in `pytorch_to_caffe.py` module

1）Abstract the `proposal_layer ()` function in the `model.py` module into a custom operator of pytorch, and create a new `proposal_layer.py` file in the same directory . the  source code as follows:

```python
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable
from nms.nms_wrapper import nms
class ProposalLayer(nn.Module):
    def __init__(self,proposal_count,nms_threshold,anchors,config):
        super(ProposalLayer, self).__init__()
        self.proposal_count=proposal_count
        self.nms_threshold=nms_threshold
        self.anchors=anchors
        self.config=config

    def forward(self,input):
        outputs = list(zip(*input))
        outputs = [torch.cat(list(o),dim=1) for o in outputs]
        rpn_class_logits,rpn_class,rpn_bbox = outputs
        inputs=[rpn_class,rpn_bbox]

        #Currently only supports batchsize 1

        inputs[0] = inputs[0].squeeze(0)
        inputs[1] = inputs[1].squeeze(0)
        ...
        return normalized_boxes
    
def apply_box_deltas(boxes, deltas):
    ...
    return resul

def clip_boxes(boxes, window):
    ...
    return boxes    
```

2）Import `proposal_layer.py` module in `pytorch_to_caffe.py` module

```python
from proposal_layer import ProposalLayer
```

3)  Add replacement function in `pytorch_to_caffe.py` module.

```python
def _proposal(self,input):
    global NET_INITTED
    NET_INITTED = False
    
    bottom_blobs=[log.blobs(input[0][0]),log.blobs(input[0][1]),log.blobs(input[0][2]),
                  log.blobs(input[1][0]),log.blobs(input[1][1]),log.blobs(input[1][2]),
                  log.blobs(input[2][0]),log.blobs(input[2][1]),log.blobs(input[2][2]),
                  log.blobs(input[3][0]),log.blobs(input[3][1]),log.blobs(input[3][2]),
                  log.blobs(input[4][0]),log.blobs(input[4][1]),log.blobs(input[4][2])]
    x = raw_proposal_forward(self,input)
    name = log.add_layer(name='proposal')
    log.add_blobs([x],name='proposal')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x)])
    layer.param.python_param.module = 'proposal_layer'
    layer.param.python_param.layer = 'ProposalLayer'

    layer.param.python_param.param_str='{"proposal_count":self.proposal_count,\
                                        "nms_threshold":self.nms_threshold}'
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

4)  Add replace operation

```python
raw_proposal_forward = ProposalLayer.forward
ProposalLayer.forward = _proposal
```

- Added support for `pyramid_roi_align` custom operator conversion in `pytorch_to_caffe.py` module

1）Abstract the `pyramid_roi_align ()` function in the `model.py` module into a custom operator of pytorch. Create a `pyramid_roi_align.py` file in the same directory .the source code as follows:

```python
import torch
import torch.nn as nn
from torch.autograd import Variable
from roialign.roi_align.crop_and_resize import CropAndResizeFunction

class PyramidRoiAlignLayer(nn.Module):
    def __init__(self,pool_size,image_shape):
        super(PyramidRoiAlignLayer,self).__init__()
        self.pool_size=pool_size
        self.image_shape=image_shape
    def forward(self,x,rois):
        inputs = [rois]+x
        for i in range(len(inputs)):
            inputs[i] = inputs[i].squeeze(0)
            
        ...
        return pooled
    
    def log2(x):
    """Implementatin of Log2. Pytorch doesn't have a native implemenation."""
        ln2 = Variable(torch.log(torch.FloatTensor([2.0])), requires_grad=False)
        return torch.log(x) / ln2     
```

2) Import `pyramid_roi_align.py` module in `pytorch_to_caffe.py` module

```python
from pyramid_roi_align import PyramidRoiAlignLayer
```

3)  Add replacement function in `pytorch_to_caffe.py` module.

```python
def _pyramid_roi_align(self,input,rois):
    global NET_INITTED
    NET_INITTED = False
    bottom_blobs = [log.blobs(input[0]),log.blobs(input[1]),log.blobs(input[2]),\
                    log.blobs(input[3]),log.blobs(rois)]
    x = raw_pyramid_roi_align(self,input,rois)
    name = log.add_layer(name='pyramid_roi_align')
    log.add_blobs([x],name='pyramid_roi_align')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x)])
    layer.param.python_param.module = 'pyramid_roi_align'
    layer.param.python_param.layer = 'PyramidRoiAlignLayer'
    NET_INITTED = True
    log.cnet.add_layer(layer)
    return x
```

4)  Add replace operation

```python
raw_pyramid_roi_align = PyramidRoiAlignLayer.forward
PyramidRoiAlignLayer.forward = _pyramid_roi_align
```

- Added support for detection_layer custom operator conversion in pytorch_to_caffe.py module

1）Abstract the `detection_layer()` function in the `model.py` module into a custom operator of pytorch. Create a `detection_layer.py` file in the same directory .the source code as follows:

```python
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable
from nms.nms_wrapper import nms

class DetectionLayer(nn.Module):
    def __init__(self,config):
        super(DetectionLayer,self).__init__()
        self.config=config
    def forward(self,rois, mrcnn_class, mrcnn_bbox, image_meta):
        rois = rois.squeeze(0)
        image_meta = image_meta.data.numpy()     
        _, _, window, _ = parse_image_meta(image_meta)
        window = window[0]
   
        detections = refine_detections(rois, mrcnn_class, mrcnn_bbox,\
                                       window, self.config)

        h, w = self.config.IMAGE_SHAPE[:2]
        scale = Variable(torch.from_numpy(np.array([h, w, h, w])).float(), \
                         requires_grad=False)
        detection_boxes = detections[:, :4] / scale
        detection_boxes = detection_boxes.unsqueeze(0)
        return detections,detection_boxes
    def refine_detections(rois, probs, deltas, window, config): 
        ...
        return result
    
    def parse_image_meta(meta):
        ...
        return image_id, image_shape, window, active_class_ids
    def apply_box_deltas(boxes, deltas):
        ...
        return result
    def clip_to_window(window, boxes):
        ...
        return boxes
    def intersect1d(tensor1, tensor2):   
        ...
        return aux[:-1][(aux[1:] == aux[:-1]).data]
    def unique1d(tensor):
        ...
        return tensor[unique_bool.data]
```

2)  Import `detection_layer.py` module in `pytorch_to_caffe.py` module

```python
from detection_layer import DetectionLayer
```

3)  Add replacement function in `pytorch_to_caffe.py` module.

```python
def _detection(self,input,mrcnn_class,mrcnn_bbox,image_meta):
    global NET_INITTED
    NET_INITTED = False
    bottom_blobs = [log.blobs(input),log.blobs(mrcnn_class),\
                    log.blobs(mrcnn_bbox),log.blobs(image_meta)]
    x,x1 = raw_detection(self,input,mrcnn_class,mrcnn_bbox,image_meta)
    name = log.add_layer(name='detection')
    log.add_blobs([x,x1],name='detection')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x),log.blobs(x1)])
    layer.param.python_param.module = 'detection_layer'
    layer.param.python_param.layer = 'detection_layer'
    NET_INITTED = True
    log.cnet.add_layer(layer)
    return x,x1
```

4)  Add replace operation

```python
raw_detection = DetectionLayer.forward
DetectionLayer.forward = _detection
```

(6) Modify the `model.py` module

- Add the `proposal_layer.py`, `pyramid_roi_align.py`, `detection_layer.py` modules in the `model.py` module

After modification：

```
from proposal_layer import ProposalLayer
from pyramid_roi_align import PyramidRoiAlignLayer
from detection_layer import DetectionLayer

...
class Classifier(nn.Module):
    def __init__(self, depth, pool_size, image_shape, num_classes):
        ...
        self.linear_bbox = nn.Linear(1024, num_classes * 4)
        self.pyramid_roi_align = PyramidRoiAlignLayer(self.pool_size,self.image_shape)
    def forward(self, x, rois):
        x = self.pyramid_roi_align(x,rois)
        x = self.conv1(x)
        ...
class Mask(nn.Module):
    def __init__(self, depth, pool_size, image_shape, num_classes):
        ...
        self.relu = nn.ReLU(inplace=True)
        self.pyramid_roi_align = PyramidRoiAlignLayer(self.pool_size,self.image_shape)
    def forward(self, x, rois):  
        x = self.pyramid_roi_align(x,rois)
        x = self.conv1(self.padding(x))
        ...
        
...

class MaskRCNN(nn.Module):
    ...
    def build(self, config):
        ...
        self.rpn = RPN(len(config.RPN_ANCHOR_RATIOS), config.RPN_ANCHOR_STRIDE, 256)  
        self.rpn_proposal=ProposalLayer(self.config.POST_NMS_ROIS_INFERENCE,\
                          self.config.RPN_NMS_THRESHOLD,self.anchors,self.config)
        self.classifier = Classifier(256, config.POOL_SIZE, config.IMAGE_SHAPE, \
                                     config.NUM_CLASSES)     
        self.detection_layer=DetectionLayer(self.config)
        # FPN Mask
        self.mask = Mask(256, config.MASK_POOL_SIZE, config.IMAGE_SHAPE, \
                         config.NUM_CLASSES)

```



- Modify the `class SamePad2d(nn.Module)` class

The input data type of the `F.pad` operator in pytorch is Int.

Before modification：

```python
class SamePad2d(nn.Module):
    ...
    def forward(self, input):
        ...
        pad_left = math.floor(pad_along_width / 2)
        pad_top = math.floor(pad_along_height / 2)
        pad_right = pad_along_width - pad_left
        pad_bottom = pad_along_height - pad_top
        return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```

After modification：

```python
class SamePad2d(nn.Module):
    ...

    def forward(self, input):
        ...
        pad_left =int(math.floor(pad_along_width / 2))
        pad_top = int(math.floor(pad_along_height / 2))
        pad_right = int(pad_along_width - pad_left)
        pad_bottom = int(pad_along_height - pad_top)
        return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```
- Add `preprocess ()` function

The image preprocessing part in the `detect ()` function of the `model.py` module is encapsulated into a `preprocess ()` function.

After modification：

```python
def preprocess(self,images):
    molded_images,image_metas,windows = self.mold_inputs(images)
    molded_images = torch.from_numpy(molded_images.transpose(0, 3, 1, 2)).float()
    molded_images = Variable(molded_images, volatile=True)
    image_metas = Variable(torch.from_numpy(image_metas))
    windows = Variable(torch.from_numpy(windows))
    return molded_images,image_metas,windows
```

- Add `forward ()` function

After modification：

```python
def forward(self,molded_images,image_metas):
    detections, mrcnn_mask = self.predict([molded_images, image_metas], mode='inference')
    return detections,mrcnn_mask
```
- Modify the `predict ()` function

Modify the `predict ()` function so that only the network inference part is retained inside the function body.

After modification：

```python
def predict(self, input, mode):
    molded_images = input[0]
    image_metas = input[1]
    
    [p2_out, p3_out, p4_out, p5_out, p6_out] = self.fpn(molded_images)
    
    rpn_feature_maps = [p2_out, p3_out, p4_out, p5_out, p6_out]
    mrcnn_feature_maps = [p2_out, p3_out, p4_out, p5_out]

    layer_outputs = []
    for p in rpn_feature_maps:
        layer_outputs.append(self.rpn(p))

    rpn_rois = self.rpn_proposal(layer_outputs)
  
    mrcnn_class_logits, mrcnn_class, mrcnn_bbox = self.classifier(mrcnn_feature_maps,
                                                                  rpn_rois)
    
    detections,detection_boxes=self.detection_layer(rpn_rois,mrcnn_class,mrcnn_bbox,
                                                    image_metas)              
    mrcnn_mask = self.mask(mrcnn_feature_maps, detection_boxes) 
    return [detections, mrcnn_mask]
```
## Start conversion

**MaskRCNN conversion depends on pytorch 0.4.0**

```python
python pytorch2caffe-mask-rcnn.py
```

After the conversion is completed, `mask-rcnn.caffemodel` and other files will be generated

**Model Conversion FAQ**

- ImportError，E.g：

```python
File "/home//pytorch2caffe-1.1.0/example/8-maskrcnn/coco.py", line 45, in <module>
    import urllib.request
ImportError: No module named request
```

Solution: Comment out the corresponding module or install the corresponding dependencies

```python
#import urllib.request
```

- Unrecognized characters, E.g:

```python
SyntaxError: Non-ASCII character '\xe2' in file /home/pytorch2caffe-1.1.0/example/8-maskrcnn/model.py
```

Solution: Add the code to the corresponding module.

```python
#coding=UTF-8
```

- Invalid syntax，E.g:：


```python
File "/home/pytorch2caffe-1.1.0/example/8-maskrcnn/model.py", line 62
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\n')
                                                                    ^
SyntaxError: invalid syntax
```

Solution: Comment out the corresponding line



## Results check

MaskRCNN contains three custom operators, which are not supported by caffe. No result verification has been performed yet.