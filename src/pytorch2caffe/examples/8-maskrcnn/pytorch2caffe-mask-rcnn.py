import coco
import model as modellib
import torch
import cv2
from convert import pytorch_to_caffe


class InferenceConfig(coco.CocoConfig):
    GPU_COUNT = 0
    IMAGES_PER_GPU = 1


config = InferenceConfig()

model = modellib.MaskRCNN(model_dir='./', config=config)
model.load_state_dict(torch.load('./mask_rcnn_coco.pth'))
print(model)


image = cv2.imread('./images/1045023827_4ec3e8ba5c_z.jpg')
molded_images, image_metas, windows = model.preprocess([image])
print('*********************start pytorch2caffe***************************')
pytorch_to_caffe.trans_net(model, [molded_images, image_metas], 'mask-rcnn')
pytorch_to_caffe.save_prototxt('./mask-rcnn.prototxt')
pytorch_to_caffe.save_caffemodel('./mask-rcnn.caffemodel')
print('*******************************done********************************')
