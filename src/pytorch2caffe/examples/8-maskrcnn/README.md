[EN](README.en.md)|CN
将pytorch框架下的MaskRCNN网络转换成caffe模型

[TOC]

## 准备工作

(1) 下载[MaskRCNN模型源码](https://github.com/multimodallearning/pytorch-mask-rcnn)，根据模型源码README下载训练好的[Pytorch模型](https://drive.google.com/open?id=1LXUgC2IZUYNEoXr05tdqyKFZY0pZyPDc)，如果无法下载，则自行训练模型后，将模型放置在统计目录下

(2)  根据模型源码README编译nms、roialign源码及配置coco数据集的python接口

(3) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-mask-rcnn.py文件到同级目录

(4) F.pad算子解决方案

MaskRCNN模型在搭建的过程中使用了`class SamePad2d(nn.Module):`类，在该类中对网络中的图像进行了Pad操作。F.pad算子caffe及D平台均不支持，解决方式有以下两种，F.pad算子的使用如下所示。

```python
F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```

方案一：

在model.py模块中删除`SamePad2d()`的应用，并根据F.pad的值为`nn.Conv2d()`及`nn.MaxPool2d()`算子添加padding参数。例如：

修改前：

```python
self.P5_conv2 = nn.Sequential(
    SamePad2d(kernel_size=3, stride=1),
    nn.Conv2d(self.out_channels, self.out_channels, kernel_size=3, stride=1),
)
```

修改后：

```python
self.P5_conv2 = nn.Sequential(
    nn.Conv2d(self.out_channels, self.out_channels, kernel_size=3, stride=1,padding=1),
)
```

方案二：

将F.pad算子转换为caffe的python层，模型转换完成之后修改mask-rcnn.prototxt文件。根据pad算子的参数值为`nn.Conv2d()`及`nn.MaxPool2d()`算子添加padding参数，并删除pad层。

pad层参数说明：

```python
layer {
  name: "pad6"
  type: "Python"
  bottom: "relu14"
  top: "pad6"
  python_param {
    param_str: "1**1**1**1"
  }
}
```

param_str中 `"1**1**1**1"`从左向右依次表示图像左右上下的padding值，与

`F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)`保持一致。

例如：

修改前：

```python
layer {
  name: "pad6"
  type: "Python"
  bottom: "relu14"
  top: "pad6"
  python_param {
    param_str: "1**1**1**1"
  }
}
layer {
  name: "conv17"
  type: "Convolution"
  bottom: "pad6"
  top: "conv17"
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 0
    kernel_size: 3
	
    ...
	
    dilation: 1
  }
}
```

修改后：

```python
layer {
  name: "conv17"
  type: "Convolution"
  bottom: "relu14"
  top: "conv17"
  convolution_param {
    num_output: 128
    bias_term: true
    pad: 1
    kernel_size: 3
    
	...
	
    dilation: 1
  }
}
```

(5) pytorch_to_caffe.py转换工具模块修改

- 注释掉pytorch_to_caffe.py模块中对interpolate算子的转换

pytorch 0.4.0版本中不含有interpolate算子，若不注释会报错。

```python
#F.interpolate = Rp(F.interpolate, _interpolate)
```

- pytorch_to_caffe.py模块中增加对pad算子转换的支持

由于不确定用户对F.pad()中pad参数的设置，暂时采用F.pad()的解决方案二，将F.pad转换成caffe的python层

1）在pytorch_to_caffe.py模块中增加替换函数

```python
def _pad(raw,input,pad,mode,value):
    x=raw(input,pad,mode,value)
    name=log.add_layer(name='pad')
    top_blobs=log.add_blobs([x],name='pad')
    layer=caffe_net.Layer_param(name=name,type='Python',
                                bottom=[log.blobs(input)],top=top_blobs)
    padding_value = str(pad[0])+'**'+str(pad[1])+'**'+str(pad[2])+'**'+str(pad[3]) 
     
    layer.param.python_param.param_str = padding_value
    log.cnet.add_layer(layer)
    return x
```

2)  增加替换操作

```python
F.pad=Rp(F.pad,_pad)
```

- pytorch_to_caffe.py模块中增加对F.upsample算子转换的支持

1）在pytorch_to_caffe.py模块中增加替换函数

```python
def _upsample(raw,input,size=None,scale_factor=None,mode='nearest',align_corners=None):
    x = raw(input,size,scale_factor,mode,align_corners)
    name = log.add_layer(name='upsample')
    top_blobs = log.add_blobs([x],name='upsample')
    layer = caffe_net.Layer_param(name=name,type='Upsample',
                                  bottom=[log.blobs(input)],top=top_blobs)
    layer.param.upsample_param.scale=scale_factor
    log.cnet.add_layer(layer)
    return x
```

2) 增加替换操作

```python
F.upsample=Rp(F.upsample,_upsample)
```

- pytorch_to_caffe.py模块中增加对proposal_layer自定义算子转换的支持

1）将model.py模块中的proposal_layer()函数抽象成pytorch的一个自定义算子，在模型源码同级目录下新建proposal_layer.py文件，内容如下；

```python
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable
from nms.nms_wrapper import nms
class ProposalLayer(nn.Module):
    def __init__(self,proposal_count,nms_threshold,anchors,config):
        super(ProposalLayer, self).__init__()
        self.proposal_count=proposal_count
        self.nms_threshold=nms_threshold
        self.anchors=anchors
        self.config=config

    def forward(self,input):
        outputs = list(zip(*input))
        outputs = [torch.cat(list(o),dim=1) for o in outputs]
        rpn_class_logits,rpn_class,rpn_bbox = outputs
        inputs=[rpn_class,rpn_bbox]

        #Currently only supports batchsize 1

        inputs[0] = inputs[0].squeeze(0)
        inputs[1] = inputs[1].squeeze(0)
        ...
        return normalized_boxes
    
def apply_box_deltas(boxes, deltas):
    ...
    return resul

def clip_boxes(boxes, window):
    ...
    return boxes    
```

2）pytorch_to_caffe.py模块中导入proposal_layer.py模块

```python
from proposal_layer import ProposalLayer
```

3)  pytorch_to_caffe.py模块中增加替换函数

```python
def _proposal(self,input):
    global NET_INITTED
    NET_INITTED = False
    
    bottom_blobs=[log.blobs(input[0][0]),log.blobs(input[0][1]),log.blobs(input[0][2]),
                  log.blobs(input[1][0]),log.blobs(input[1][1]),log.blobs(input[1][2]),
                  log.blobs(input[2][0]),log.blobs(input[2][1]),log.blobs(input[2][2]),
                  log.blobs(input[3][0]),log.blobs(input[3][1]),log.blobs(input[3][2]),
                  log.blobs(input[4][0]),log.blobs(input[4][1]),log.blobs(input[4][2])]
    x = raw_proposal_forward(self,input)
    name = log.add_layer(name='proposal')
    log.add_blobs([x],name='proposal')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x)])
    layer.param.python_param.module = 'proposal_layer'
    layer.param.python_param.layer = 'ProposalLayer'

    layer.param.python_param.param_str='{"proposal_count":self.proposal_count,\
                                        "nms_threshold":self.nms_threshold}'
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

4) pytorch_to_caffe.py模块中增加替换操作

```python
raw_proposal_forward = ProposalLayer.forward
ProposalLayer.forward = _proposal
```

- pytorch_to_caffe.py模块中增加对pyramid_roi_align自定义算子转换的支持

1）将model.py模块中的pyramid_roi_align()函数抽象成pytorch的一个自定义算子，在模型源码同级目录下新建pyramid_roi_align.py文件，内容如下；

```python
import torch
import torch.nn as nn
from torch.autograd import Variable
from roialign.roi_align.crop_and_resize import CropAndResizeFunction

class PyramidRoiAlignLayer(nn.Module):
    def __init__(self,pool_size,image_shape):
        super(PyramidRoiAlignLayer,self).__init__()
        self.pool_size=pool_size
        self.image_shape=image_shape
    def forward(self,x,rois):
        inputs = [rois]+x
        for i in range(len(inputs)):
            inputs[i] = inputs[i].squeeze(0)
            
        ...
        return pooled
    
    def log2(x):
    """Implementatin of Log2. Pytorch doesn't have a native implemenation."""
        ln2 = Variable(torch.log(torch.FloatTensor([2.0])), requires_grad=False)
        return torch.log(x) / ln2     
```

2)  pytorch_to_caffe.py模块中导入pyramid_roi_align.py模块

```python
from pyramid_roi_align import PyramidRoiAlignLayer
```

3)  pytorch_to_caffe.py模块中增加替换函数

```python
def _pyramid_roi_align(self,input,rois):
    global NET_INITTED
    NET_INITTED = False
    bottom_blobs = [log.blobs(input[0]),log.blobs(input[1]),log.blobs(input[2]),\
                    log.blobs(input[3]),log.blobs(rois)]
    x = raw_pyramid_roi_align(self,input,rois)
    name = log.add_layer(name='pyramid_roi_align')
    log.add_blobs([x],name='pyramid_roi_align')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x)])
    layer.param.python_param.module = 'pyramid_roi_align'
    layer.param.python_param.layer = 'PyramidRoiAlignLayer'
    NET_INITTED = True
    log.cnet.add_layer(layer)
    return x
```

4) pytorch_to_caffe.py模块中增加替换操作

```python
raw_pyramid_roi_align = PyramidRoiAlignLayer.forward
PyramidRoiAlignLayer.forward = _pyramid_roi_align
```

- pytorch_to_caffe.py模块中增加对detection_layer自定义算子转换的支持

1）将model.py模块中的detection_layer()函数抽象成pytorch的一个自定义算子，在模型源码同级目录下新建detection_layer.py文件，内容如下；

```python
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable
from nms.nms_wrapper import nms

class DetectionLayer(nn.Module):
    def __init__(self,config):
        super(DetectionLayer,self).__init__()
        self.config=config
    def forward(self,rois, mrcnn_class, mrcnn_bbox, image_meta):
        rois = rois.squeeze(0)
        image_meta = image_meta.data.numpy()     
        _, _, window, _ = parse_image_meta(image_meta)
        window = window[0]
   
        detections = refine_detections(rois, mrcnn_class, mrcnn_bbox,\
                                       window, self.config)

        h, w = self.config.IMAGE_SHAPE[:2]
        scale = Variable(torch.from_numpy(np.array([h, w, h, w])).float(), \
                         requires_grad=False)
        detection_boxes = detections[:, :4] / scale
        detection_boxes = detection_boxes.unsqueeze(0)
        return detections,detection_boxes
    def refine_detections(rois, probs, deltas, window, config): 
        ...
        return result
    
    def parse_image_meta(meta):
        ...
        return image_id, image_shape, window, active_class_ids
    def apply_box_deltas(boxes, deltas):
        ...
        return result
    def clip_to_window(window, boxes):
        ...
        return boxes
    def intersect1d(tensor1, tensor2):   
        ...
        return aux[:-1][(aux[1:] == aux[:-1]).data]
    def unique1d(tensor):
        ...
        return tensor[unique_bool.data]
```

2)  pytorch_to_caffe.py模块中导入detection_layer.py模块

```python
from detection_layer import DetectionLayer
```

3)  pytorch_to_caffe.py模块中增加替换函数

```python
def _detection(self,input,mrcnn_class,mrcnn_bbox,image_meta):
    global NET_INITTED
    NET_INITTED = False
    bottom_blobs = [log.blobs(input),log.blobs(mrcnn_class),\
                    log.blobs(mrcnn_bbox),log.blobs(image_meta)]
    x,x1 = raw_detection(self,input,mrcnn_class,mrcnn_bbox,image_meta)
    name = log.add_layer(name='detection')
    log.add_blobs([x,x1],name='detection')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=bottom_blobs,top=[log.blobs(x),log.blobs(x1)])
    layer.param.python_param.module = 'detection_layer'
    layer.param.python_param.layer = 'detection_layer'
    NET_INITTED = True
    log.cnet.add_layer(layer)
    return x,x1
```

4) pytorch_to_caffe.py模块中增加替换操作

```python
raw_detection = DetectionLayer.forward
DetectionLayer.forward = _detection
```

(6) model.py模块的修改

- 添加proposal_layer.py、pyramid_roi_align.py、detection_layer.py模块

修改后：

```
from proposal_layer import ProposalLayer
from pyramid_roi_align import PyramidRoiAlignLayer
from detection_layer import DetectionLayer

...
class Classifier(nn.Module):
    def __init__(self, depth, pool_size, image_shape, num_classes):
        ...
        self.linear_bbox = nn.Linear(1024, num_classes * 4)
        self.pyramid_roi_align = PyramidRoiAlignLayer(self.pool_size,self.image_shape)
    def forward(self, x, rois):
        x = self.pyramid_roi_align(x,rois)
        x = self.conv1(x)
        ...
class Mask(nn.Module):
    def __init__(self, depth, pool_size, image_shape, num_classes):
        ...
        self.relu = nn.ReLU(inplace=True)
        self.pyramid_roi_align = PyramidRoiAlignLayer(self.pool_size,self.image_shape)
    def forward(self, x, rois):  
        x = self.pyramid_roi_align(x,rois)
        x = self.conv1(self.padding(x))
        ...
        
...

class MaskRCNN(nn.Module):
    ...
    def build(self, config):
        ...
        self.rpn = RPN(len(config.RPN_ANCHOR_RATIOS), config.RPN_ANCHOR_STRIDE, 256)  
        self.rpn_proposal=ProposalLayer(self.config.POST_NMS_ROIS_INFERENCE,\
                          self.config.RPN_NMS_THRESHOLD,self.anchors,self.config)
        self.classifier = Classifier(256, config.POOL_SIZE, config.IMAGE_SHAPE, \
                                     config.NUM_CLASSES)     
        self.detection_layer=DetectionLayer(self.config)
        # FPN Mask
        self.mask = Mask(256, config.MASK_POOL_SIZE, config.IMAGE_SHAPE, \
                         config.NUM_CLASSES)

```



- 修改`class SamePad2d(nn.Module)`类

pytorch中F.pad算子输入数据类型为Int类型。

修改前：

```python
class SamePad2d(nn.Module):
    ...
    def forward(self, input):
        ...
        pad_left = math.floor(pad_along_width / 2)
        pad_top = math.floor(pad_along_height / 2)
        pad_right = pad_along_width - pad_left
        pad_bottom = pad_along_height - pad_top
        return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```

修改后：

```python
class SamePad2d(nn.Module):
    ...

    def forward(self, input):
        ...
        pad_left =int(math.floor(pad_along_width / 2))
        pad_top = int(math.floor(pad_along_height / 2))
        pad_right = int(pad_along_width - pad_left)
        pad_bottom = int(pad_along_height - pad_top)
        return F.pad(input, (pad_left, pad_right, pad_top, pad_bottom), 'constant', 0)
```
- 编写preprocess()函数

将model.py模块detect()函数中图像预处理部分写成preprocess()函数。

修改后：

```python
def preprocess(self,images):
    molded_images,image_metas,windows = self.mold_inputs(images)
    molded_images = torch.from_numpy(molded_images.transpose(0, 3, 1, 2)).float()
    molded_images = Variable(molded_images, volatile=True)
    image_metas = Variable(torch.from_numpy(image_metas))
    windows = Variable(torch.from_numpy(windows))
    return molded_images,image_metas,windows
```

- 新建模型推理forward()函数

修改后：

```python
def forward(self,molded_images,image_metas):
    detections, mrcnn_mask = self.predict([molded_images, image_metas], mode='inference')
    return detections,mrcnn_mask
```
- 修改predict()函数

修改predict()函数，函数体内部只保留网络推理部分。

修改后：

```python
def predict(self, input, mode):
    molded_images = input[0]
    image_metas = input[1]
    
    [p2_out, p3_out, p4_out, p5_out, p6_out] = self.fpn(molded_images)
    
    rpn_feature_maps = [p2_out, p3_out, p4_out, p5_out, p6_out]
    mrcnn_feature_maps = [p2_out, p3_out, p4_out, p5_out]

    layer_outputs = []
    for p in rpn_feature_maps:
        layer_outputs.append(self.rpn(p))

    rpn_rois = self.rpn_proposal(layer_outputs)
  
    mrcnn_class_logits, mrcnn_class, mrcnn_bbox = self.classifier(mrcnn_feature_maps,
                                                                  rpn_rois)
    
    detections,detection_boxes=self.detection_layer(rpn_rois,mrcnn_class,mrcnn_bbox,
                                                    image_metas)              
    mrcnn_mask = self.mask(mrcnn_feature_maps, detection_boxes) 
    return [detections, mrcnn_mask]
```
## 开始转换

**MaskRCNN转换依赖pytorch 0.4.0**

```python
python pytorch2caffe-mask-rcnn.py
```

执行该命令会生成mask-rcnn.caffemodel等文件

**模型转换FAQ**

- ImportError，例如：

```python
File "/home//pytorch2caffe-1.1.0/example/8-maskrcnn/coco.py", line 45, in <module>
    import urllib.request
ImportError: No module named request
```

解决方式：将相应模块注释掉，或者安装相应的依赖

```python
#import urllib.request
```

- 无法识别字符，例如：

```python
SyntaxError: Non-ASCII character '\xe2' in file /home/pytorch2caffe-1.1.0/example/8-maskrcnn/model.py
```

解决方式：相应模块添加一下代码

```python
#coding=UTF-8
```

python语法错误，例如：

```python
File "/home/pytorch2caffe-1.1.0/example/8-maskrcnn/model.py", line 62
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\n')
                                                                    ^
SyntaxError: invalid syntax
```

解决方式：注释掉相应行即可



## 结果校验

MaskRCNN中含有三个自定义算子，caffe不支持，暂未进行结果校验。