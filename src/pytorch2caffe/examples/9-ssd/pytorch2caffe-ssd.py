from ssd import build_ssd
import torch
from torch.autograd import Variable
from convert import pytorch_to_caffe

ssd_net = build_ssd('test', 300, 21)
ssd_net.load_weights('./weights/ssd_300_VOC0712.pth')
ssd_net.eval()

input_var = Variable(torch.rand([1, 3, 300, 300]))
print('****************start pytorch2caffe*****************')
pytorch_to_caffe.trans_net(ssd_net, [input_var], 'ssd')
pytorch_to_caffe.save_prototxt('ssd.prototxt')
pytorch_to_caffe.save_caffemodel('ssd.caffemodel')
print('*************************done***********************')
