from ssd import build_ssd
import torch
import numpy as np
from torch.autograd import Variable
import caffe


input_size = [1, 3, 300, 300]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

ssd_net = build_ssd('test', 300, 21)
ssd_net.load_weights('./weights/ssd_300_VOC0712.pth')
ssd_net.eval()
input_var = Variable(torch.from_numpy(input_data))
output_var, loc, conf = ssd_net(input_var)


caffe_model = './ssd.prototxt'
caffe_weights = './ssd.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_var.data.cpu().numpy()
caffe_out = caffe_net.forward()

priors = caffe_net.blobs['cat3'].data
diff_loc = np.abs(loc.data.cpu().numpy().flatten() - caffe_net.blobs['cat1'].data.flatten())
diff_conf = np.abs(conf.data.cpu().numpy().flatten() - caffe_net.blobs['softmax1'].data.flatten())

print('diff_loc:')
print('diff_max:', diff_loc.max(), '    ', 'diff_mean:', diff_loc.mean(), '    ',
      'diff_min:', diff_loc.min(), '    ', 'diff_median:', np.median(diff_loc))
print('diff_conf:')
print('diff_max:', diff_conf.max(), '    ', 'diff_mean:', diff_conf.mean(), '    ',
      'diff_min:', diff_conf.min(), '    ', 'diff_median:', np.median(diff_conf))
