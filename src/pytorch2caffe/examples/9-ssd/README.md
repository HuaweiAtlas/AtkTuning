[EN](README.en.md)|CN
将pytorch框架下的ssd模型转换成caffe模型

[TOC]

## 准备工作

(1)  下载[ssd模型源码](https://github.com/amdegroot/ssd.pytorch)

(2)  根据ssd源码的README，下载训练好的[ssd模型](https://s3.amazonaws.com/amdegroot-models/ssd_300_VOC0712.pth)。新建weights目录并将模型放置在该目录下，如果模型无法下载，则需要训练一个ssd模型。

(3)  拷贝工具中的Caffe、convert文件夹及pytorch2caffe-ssd.py、test-ssd.py文件到同级目录

(4)  pytorch_to_caffe.py模块增加对自定义算子的转换及ssd源码修改

- **pytorch_to_caffe.py模块中增加对l2norm自定义算子转换的支持**

1）导入L2Norm自定义算子

```python
from layers.modules.l2norm import L2Norm
```

2)   增加替换函数

```python
def _l2norm(self, input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_L2Norm_forward(self,input)
    bottom_blobs=[log.blobs(input)]
    name = log.add_layer(name='l2norm')
    top_blobs = log.add_blobs([x], name='l2norm')
    layer = caffe_net.Layer_param(name=name, type='Normalize',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.norm_param.across_spatial = False
    layer.param.norm_param.channel_shared = False
    layer.param.norm_param.scale_filler.type= 'constant'
    layer.param.norm_param.scale_filler.value= self.gamma   
    layer.add_data(self.weight.cpu().data.numpy())
    
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3)   增加替换操作

```python
raw_L2Norm_forward = L2Norm.forward
L2Norm.forward = _l2norm
```

- **pytorch_to_caffe.py模块中增加对Detect自定义算子转换的支持**

1）导入Detect自定义算子

```python
from layers.functions.detection import Detect
```

2)   增加替换函数

```python
def _detect(self, loc_data, conf_data, prior_data):
    global NET_INITTED
    NET_INITTED = False
    x = raw_Detect_forward(self, loc_data, conf_data, prior_data)

    bottom_blobs=[log.blobs(loc_data),log.blobs(conf_data),log.blobs(prior_data)]
    name = log.add_layer(name='detect')
    top_blobs = log.add_blobs([x], name='detect')
    layer = caffe_net.Layer_param(name=name, type='DetectionOutput',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.detection_output_param.num_classes = self.num_classes
    layer.param.detection_output_param.background_label_id = self.background_label
    layer.param.detection_output_param.nms_param.nms_threshold= self.nms_thresh
    layer.param.detection_output_param.nms_param.top_k= 400
    layer.param.detection_output_param.code_type = 2
    layer.param.detection_output_param.keep_top_k = self.top_k
    layer.param.detection_output_param.confidence_threshold = self.conf_thresh

    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3)   增加替换操作

```python
raw_Detect_forward = Detect.forward
Detect.forward = _detect
```

- **pytorch_to_caffe.py模块中增加对Priorbox1自定义算子转换的支持**

1)   复制layers/functions/目录下prior_box.py并重命名为priorbox.py，修改部分如下：

```python
from __future__ import division
from torch.autograd import Function
from math import sqrt as sqrt
from itertools import product as product
import torch


class PriorBox1(Function):

    ...
	
    def forward(self, feat, data):
        mean = []
        shape = feat.size()
        for i in range(6):
            if shape[2] == self.feature_maps[i]:
                k = i
        
		for i, j in product(range(self.feature_maps[k]), repeat=2):
            f_k = self.image_size / self.steps[k]
            # unit center x,y
            cx = (j + 0.5) / f_k
            cy = (i + 0.5) / f_k

            ...
		
        output = torch.Tensor(mean).view(-1, 4)
        if self.clip:
            output.clamp_(max=1, min=0)
        return output
```

2)  导入PriorBox1自定义算子

```python
from layers.functions.priorbox import PriorBox1
```

3）增加替换函数

```python
def _priorbox(self, feat, data):
    global NET_INITTED
    NET_INITTED = False
    x = raw_PriorBox_forward(self, feat, data)
    shape = feat.size()
    for i in range(6):
        if self.feature_maps[i] == shape[2]:
            k = i

    bottom_blobs=[log.blobs(feat),log.blobs(data)]
    name = log.add_layer(name='priorbox')
    top_blobs = log.add_blobs([x], name='priorbox')
    layer = caffe_net.Layer_param(name=name, type='PriorBox',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.prior_box_param.min_size.extend([self.min_sizes[k]])
    layer.param.prior_box_param.max_size.extend([self.max_sizes[k]])
    layer.param.prior_box_param.aspect_ratio.extend(self.aspect_ratios[k])
    layer.param.prior_box_param.variance.extend([0.1,0.1,0.2,0.2])
    layer.param.prior_box_param.clip = self.clip
    layer.param.prior_box_param.step = self.steps[k]
    layer.param.prior_box_param.offset = 0.5

    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

4)  增加替换操作

```python
raw_PriorBox_forward = PriorBox1.forward
PriorBox1.forward = _priorbox
```

- **修改ssd.py模块**

修改后：

```python
class SSD(nn.Module):
    ...
    def __init__(self, phase, size, base, extras, head, num_classes):
        ...
        self.priorbox = PriorBox(self.cfg)
        self.priorbox1 = PriorBox1(self.cfg)
        self.priors = Variable(self.priorbox.forward(), volatile=True)
        ...
        
    def forward(self, x):
        ...
        conf = list()
        priors = list()
        input = x
        ...
        for (x, l, c) in zip(sources, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())
        
        for k,x in enumerate(sources):
            priors.append(self.priorbox1(x,input))

        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        priors = torch.cat([o for o in priors],0)
        conf = self.softmax(conf.view(conf.size(0),-1,self.num_classes))

        if self.phase == "test":
            output = self.detect(
                loc,                   # loc preds
                conf.view(conf.size(0), -1),                # conf preds
                priors          # default boxes
            )
        else:
            ...
        return output,loc,conf
        
```

- **修改layers/functions/目录下的detection.py模块**

修改前：

```python
class Detect(Function):
    ...
    def forward(self, loc_data, conf_data, prior_data):
        ...
        num = loc_data.size(0)  # batch size
        num_priors = prior_data.size(0)
        ...
```

修改后：

```python
class Detect(Function):
    ...
    def forward(self, loc_data, conf_data, prior_data):
        ...
        loc_data = loc_data.view(loc_data.size(0),-1,4)
        conf_data = conf_data.view(conf_data.size(0),-1,self.num_classes)
        num = loc_data.size(0)  # batch size
        num_priors = prior_data.size(0)
        ...
```

- **修改layers/functions/目录下的init.py模块**

修改前：

```python
from .detection import Detect
from .prior_box import PriorBox
__all__ = ['Detect', 'PriorBox'']
```

修改后：

```python
from .detection import Detect
from .prior_box import PriorBox
from .priorbox import PriorBox1
__all__ = ['Detect', 'PriorBox','PriorBox1']
```



## 开始转换

```python
python pytorch2caffe-ssd.py
```

执行该命令会生成ssd.caffemodel等文件

## 结果校验

- 修改ssd.prototxt文件的cat3层参数

将axis的值改为2，因为pytorch中priorbox算子与caffe里面priorbox算子对数据的维度要求不同。

修改前：

```python
layer {
  name: "cat3"
  type: "Concat"
  bottom: "priorbox1"
  bottom: "priorbox2"
  bottom: "priorbox3"
  bottom: "priorbox4"
  bottom: "priorbox5"
  bottom: "priorbox6"
  top: "cat3"
  concat_param {
    axis: 0
  }
}
```

修改后：

```python
layer {
  name: "cat3"
  type: "Concat"
  bottom: "priorbox1"
  bottom: "priorbox2"
  bottom: "priorbox3"
  bottom: "priorbox4"
  bottom: "priorbox5"
  bottom: "priorbox6"
  top: "cat3"
  concat_param {
    axis: 2
  }
}
```

- 结果校验

```python
python test-ssd.py
```

## ssd的caffe2D转换

- 修改ssd.prototxt文件中的view层

将除了view13层之外的view层的类型由Reshape改为Flatten，不然omg会失败。原因是D平台对ssd进行了加速，使用了算子融合，算子融合时采用的是Flatten类型。

例如，修改前：

```python
 layer {
  name: "view1"
  type: "Reshape"
  bottom: "permute1"
  top: "view1"
  reshape_param {
    shape {
      dim: 1
      dim: -1
    }
  }
}
```

修改后：

```python
pythonlayer {
  name: "view1"
  type: "Flatten"
  bottom: "permute1"
  top: "view1"
  flatten_param {
    axis: 1
}
```

- 修改ssd.prototxt文件中的detect层

将detect1层的类型由DetectionOutput修改为SSDDetectionOutput。原因是D平台的DetectionOutput算子分为FasterRCNN模型和SSD模型两种应用场景，需要具体明确。

修改前：

```python
layer {
  name: "detect1"
  type: "DetectionOutput"
  bottom: "cat1"
  bottom: "view14"
  bottom: "cat3"
  top: "detect1"
  detection_output_param {
    num_classes: 21
    background_label_id: 0
    nms_param {
      nms_threshold: 0.44999999
      top_k: 400
    }
    code_type: CENTER_SIZE
    keep_top_k: 200
    confidence_threshold: 0.0099999998
  }
}
```

修改后：

```python
layer {
  name: "detect1"
  type: "SSDDetectionOutput"
  bottom: "cat1"
  bottom: "view14"
  bottom: "cat3"
  top: "detect1"
  detection_output_param {
    num_classes: 21
    background_label_id: 0
    nms_param {
      nms_threshold: 0.44999999
      top_k: 400
    }
    code_type: CENTER_SIZE
    keep_top_k: 200
    confidence_threshold: 0.0099999998
  }
}
```

