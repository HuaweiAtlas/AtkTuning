EN|[CN](README.md)

Converting the SSD Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [SSD model source code](https://github.com/amdegroot/ssd.pytorch).

(2) Download the trained [SSD model](https://s3.amazonaws.com/amdegroot-models/ssd_300_VOC0712.pth) by following the README file of the SSD source code. Create the weights directory and store the model in the directory. If the model cannot be downloaded, train an SSD model.

(3) Copy the Caffe and convert folders and the pytorch2caffe-ssd.py and test-ssd.py files to the same directory.

(4) Add the custom operator conversion and SSD source code modification the pytorch_to_caffe.py module.

- **Modify the pytorch_to_caffe.py module to support the conversion of the l2norm custom operator.**

1) Import the L2Norm custom operator.

```python
from layers.modules.l2norm import L2Norm
```

2) Add a replacement function.

```python
def _l2norm(self, input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_L2Norm_forward(self,input)
    bottom_blobs=[log.blobs(input)]
    name = log.add_layer(name='l2norm')
    top_blobs = log.add_blobs([x], name='l2norm')
    layer = caffe_net.Layer_param(name=name, type='Normalize',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.norm_param.across_spatial = False
    layer.param.norm_param.channel_shared = False
    layer.param.norm_param.scale_filler.type= 'constant'
    layer.param.norm_param.scale_filler.value= self.gamma   
    layer.add_data(self.weight.cpu().data.numpy())
    
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3) Add a replacement operation.

```python
raw_L2Norm_forward = L2Norm.forward
L2Norm.forward = _l2norm
```

- **Modify the pytorch_to_caffe.py module to support the conversion of the Detect custom operator.**

1) Import the Detect custom operator.

```python
from layers.functions.detection import Detect
```

2) Add a replacement function.

```python
def _detect(self, loc_data, conf_data, prior_data):
    global NET_INITTED
    NET_INITTED = False
    x = raw_Detect_forward(self, loc_data, conf_data, prior_data)

    bottom_blobs=[log.blobs(loc_data),log.blobs(conf_data),log.blobs(prior_data)]
    name = log.add_layer(name='detect')
    top_blobs = log.add_blobs([x], name='detect')
    layer = caffe_net.Layer_param(name=name, type='DetectionOutput',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.detection_output_param.num_classes = self.num_classes
    layer.param.detection_output_param.background_label_id = self.background_label
    layer.param.detection_output_param.nms_param.nms_threshold= self.nms_thresh
    layer.param.detection_output_param.nms_param.top_k= 400
    layer.param.detection_output_param.code_type = 2
    layer.param.detection_output_param.keep_top_k = self.top_k
    layer.param.detection_output_param.confidence_threshold = self.conf_thresh

    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3) Add a replacement operation.

```python
raw_Detect_forward = Detect.forward
Detect.forward = _detect
```

- **Modify the pytorch_to_caffe.py module  to support the conversion of the Priorbox1 custom operator.**

1) Copy prior_box.py in the layers/functions/ directory and rename it priorbox.py.

```python
from __future__ import division
from torch.autograd import Function
from math import sqrt as sqrt
from itertools import product as product
import torch


class PriorBox1(Function):

    ...
	
    def forward(self, feat, data):
        mean = []
        shape = feat.size()
        for i in range(6):
            if shape[2] == self.feature_maps[i]:
                k = i
        
		for i, j in product(range(self.feature_maps[k]), repeat=2):
            f_k = self.image_size / self.steps[k]
            # unit center x,y
            cx = (j + 0.5) / f_k
            cy = (i + 0.5) / f_k

            ...
		
        output = torch.Tensor(mean).view(-1, 4)
        if self.clip:
            output.clamp_(max=1, min=0)
        return output
```

2) Import the PriorBox1 custom operator.

```python
from layers.functions.priorbox import PriorBox1
```

3) Add a replacement function.

```python
def _priorbox(self, feat, data):
    global NET_INITTED
    NET_INITTED = False
    x = raw_PriorBox_forward(self, feat, data)
    shape = feat.size()
    for i in range(6):
        if self.feature_maps[i] == shape[2]:
            k = i

    bottom_blobs=[log.blobs(feat),log.blobs(data)]
    name = log.add_layer(name='priorbox')
    top_blobs = log.add_blobs([x], name='priorbox')
    layer = caffe_net.Layer_param(name=name, type='PriorBox',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.prior_box_param.min_size.extend([self.min_sizes[k]])
    layer.param.prior_box_param.max_size.extend([self.max_sizes[k]])
    layer.param.prior_box_param.aspect_ratio.extend(self.aspect_ratios[k])
    layer.param.prior_box_param.variance.extend([0.1,0.1,0.2,0.2])
    layer.param.prior_box_param.clip = self.clip
    layer.param.prior_box_param.step = self.steps[k]
    layer.param.prior_box_param.offset = 0.5

    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

4) Added a replacement operation.

```python
raw_PriorBox_forward = PriorBox1.forward
PriorBox1.forward = _priorbox
```

- **Modify the ssd.py module.**

After modification:

```python
class SSD(nn.Module):
    ...
    def __init__(self, phase, size, base, extras, head, num_classes):
        ...
        self.priorbox = PriorBox(self.cfg)
        self.priorbox1 = PriorBox1(self.cfg)
        self.priors = Variable(self.priorbox.forward(), volatile=True)
        ...
        
    def forward(self, x):
        ...
        conf = list()
        priors = list()
        input = x
        ...
        for (x, l, c) in zip(sources, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())
        
        for k,x in enumerate(sources):
            priors.append(self.priorbox1(x,input))

        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        priors = torch.cat([o for o in priors],0)
        conf = self.softmax(conf.view(conf.size(0),-1,self.num_classes))

        if self.phase == "test":
            output = self.detect(
                loc,                   # loc preds
                conf.view(conf.size(0), -1),                # conf preds
                priors          # default boxes
            )
        else:
            ...
        return output,loc,conf
        
```

- **Modify the detection.py module in the layers/functions/ directory.**

Before modification:

```python
class Detect(Function):
    ...
    def forward(self, loc_data, conf_data, prior_data):
        ...
        num = loc_data.size(0)  # batch size
        num_priors = prior_data.size(0)
        ...
```

After modification:

```python
class Detect(Function):
    ...
    def forward(self, loc_data, conf_data, prior_data):
        ...
        loc_data = loc_data.view(loc_data.size(0),-1,4)
        conf_data = conf_data.view(conf_data.size(0),-1,self.num_classes)
        num = loc_data.size(0)  # batch size
        num_priors = prior_data.size(0)
        ...
```

- **Modify the init.py module in the layers/functions/ directory.**

Before modification:

```python
from .detection import Detect
from .prior_box import PriorBox
__all__ = ['Detect', 'PriorBox'']
```

After modification:

```python
from .detection import Detect
from .prior_box import PriorBox
from .priorbox import PriorBox1
__all__ = ['Detect', 'PriorBox','PriorBox1']
```



## Conversion

```python
python pytorch2caffe-ssd.py
```

After this command is executed, files such as ssd.caffemodel are generated.

## Result Verification

- Modify the cat3 layer parameter in the ssd.prototxt file.

Change the value of axis to 2 because the priorbox operator in PyTorch and the priorbox operator in Caffe have different requirements on data dimensions.

Before modification:

```python
layer {
  name: "cat3"
  type: "Concat"
  bottom: "priorbox1"
  bottom: "priorbox2"
  bottom: "priorbox3"
  bottom: "priorbox4"
  bottom: "priorbox5"
  bottom: "priorbox6"
  top: "cat3"
  concat_param {
    axis: 0
  }
}
```

After modification:

```python
layer {
  name: "cat3"
  type: "Concat"
  bottom: "priorbox1"
  bottom: "priorbox2"
  bottom: "priorbox3"
  bottom: "priorbox4"
  bottom: "priorbox5"
  bottom: "priorbox6"
  top: "cat3"
  concat_param {
    axis: 2
  }
}
```

- Result Verification

```python
python test-ssd.py
```

## SSD to Caffe2D Conversion

- Modify the view layer in the ssd.prototxt file.

Change the type of view layers except view 13 from Reshape to Flatten. Otherwise, the OMG fails. The reason is that the D platform accelerates the SSD and uses operator fusion. The Flatten type is used during operator fusion.

Before modification:

```python
 layer {
  name: "view1"
  type: "Reshape"
  bottom: "permute1"
  top: "view1"
  reshape_param {
    shape {
      dim: 1
      dim: -1
    }
  }
}
```

After modification:

```python
pythonlayer {
  name: "view1"
  type: "Flatten"
  bottom: "permute1"
  top: "view1"
  flatten_param {
    axis: 1
}
```

- Modify the detect layer in the ssd.prototxt file.

The type of the detect1 layer is changed from DetectionOutput to SSDDetectionOutput. The reason is that the DetectionOutput operator of the D platform has two application scenarios: Faster R-CNN model and SSD model.

Before modification:

```python
layer {
  name: "detect1"
  type: "DetectionOutput"
  bottom: "cat1"
  bottom: "view14"
  bottom: "cat3"
  top: "detect1"
  detection_output_param {
    num_classes: 21
    background_label_id: 0
    nms_param {
      nms_threshold: 0.44999999
      top_k: 400
    }
    code_type: CENTER_SIZE
    keep_top_k: 200
    confidence_threshold: 0.0099999998
  }
}
```

After modification:

```python
layer {
  name: "detect1"
  type: "SSDDetectionOutput"
  bottom: "cat1"
  bottom: "view14"
  bottom: "cat3"
  top: "detect1"
  detection_output_param {
    num_classes: 21
    background_label_id: 0
    nms_param {
      nms_threshold: 0.44999999
      top_k: 400
    }
    code_type: CENTER_SIZE
    keep_top_k: 200
    confidence_threshold: 0.0099999998
  }
}
```

