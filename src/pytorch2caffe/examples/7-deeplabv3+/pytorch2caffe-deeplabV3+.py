from modeling.deeplab import DeepLab
import torch
from torch.autograd import Variable
from convert import pytorch_to_caffe

net = DeepLab(backbone='resnet')
net.eval()

input_var = Variable(torch.rand([1, 3, 512, 512]))
print('*****************start pytorch2caffe*****************')
pytorch_to_caffe.trans_net(net, [input_var], 'deeplabv3+')
pytorch_to_caffe.save_prototxt('deeplabv3+.prototxt')
pytorch_to_caffe.save_caffemodel('deeplabv3+.caffemodel')
print('**********************done***************************')
torch.save(net.state_dict(), './deeplabv3+.pth')
