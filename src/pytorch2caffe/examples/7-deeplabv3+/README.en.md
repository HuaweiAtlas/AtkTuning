EN|[CN](README.md)
Converting the DeepLabv3+ Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [DeepLabv3+ model source code](https://github.com/jfzhang95/pytorch-deeplab-xception).

(2) In this example, an untrained random initialization model is used to demonstrate the pytorch2caffe process. In actual application, the trained PyTorch model can be converted based on this example. 

(3) Modify the asp.py file in the modeling directory.

Caffe does not have the adaptive average pooling operator. During model conversion, the adaptive average pooling operator in the PyTorch model is converted into the average pooling operator, and the parameter of the adaptive average pooling operator cannot be tuple data.

Before modification:

```python
 self.global_avg_pool = nn.Sequential(nn.AdaptiveAvgPool2d((1,1)),
                                         nn.Conv2d(inplanes, 256, 1, stride=1, 
                                                   bias=False),
                                         BatchNorm(256),
                                         nn.ReLU())
```

After modification:

```python
   self.global_avg_pool = nn.Sequential(nn.AdaptiveAvgPool2d((1)),
                                         nn.Conv2d(inplanes, 256, 1, stride=1, 
                                                   bias=False),
                                         BatchNorm(256),
                                         nn.ReLU())
```
(4) Copy the Caffe and convert folders and the pytorch2caffe-deeplabv3+.py and test-deeplabv3+.py files to the same directory.

## Conversion

```python
python pytorch2caffe-deeplabv3+.py
```

After this command is executed, files such as deeplabv3+.caffemodel are generated.

## Result Verification

```python
python test-deeplabv3+.py
```









