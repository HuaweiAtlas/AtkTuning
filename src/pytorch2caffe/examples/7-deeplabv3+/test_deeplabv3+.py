import torch
import numpy as np
from torch.autograd import Variable
from modeling.deeplab import DeepLab
import caffe


input_size = [1, 3, 512, 512]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

input_var = Variable(torch.from_numpy(input_data))
net = DeepLab(backbone='resnet')
net.load_state_dict(torch.load('./deeplabv3+.pth', map_location=lambda storage, loc: storage))
net.eval()
pytorch_output = net(input_var)

caffe_model = './deeplabv3+.prototxt'
caffe_weights = './deeplabv3+.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()

diff_upsample3 = np.abs(pytorch_output.data.cpu().numpy().flatten()
                        - caffe_out['upsample3'].flatten())
print('diff:')
print('diff_max:', diff_upsample3.max(), '    ', 'diff_mean:', diff_upsample3.mean(), '    ',
      'diff_min:', diff_upsample3.min(), '    ', 'diff_median:', np.median(diff_upsample3))
