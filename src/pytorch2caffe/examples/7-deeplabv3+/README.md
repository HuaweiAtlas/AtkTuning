[EN](README.en.md)|CN
将pytorch框架下的deeplabv3+模型转换成caffe模型

[TOC]

## 准备工作

(1) 下载[deeplabv3+模型源码](https://github.com/jfzhang95/pytorch-deeplab-xception)

(2)  此示例采用未经训练的随机初始化模型演示pytorch2caffe过程，实际应用中可根据该示例转换训练好的pytorch模型。 

(3)  修改modeling目录下的aspp.py文件

caffe不具有自适应平均池化算子，在模型转换的时候将pytorch模型中的自适应平均池化转换成平均池化算子，转换过程中不支持自适应平均池化算子的参数为tuple类型数据。

修改前：

```python
 self.global_avg_pool = nn.Sequential(nn.AdaptiveAvgPool2d((1,1)),
                                         nn.Conv2d(inplanes, 256, 1, stride=1, 
                                                   bias=False),
                                         BatchNorm(256),
                                         nn.ReLU())
```

修改后：

```python
   self.global_avg_pool = nn.Sequential(nn.AdaptiveAvgPool2d((1)),
                                         nn.Conv2d(inplanes, 256, 1, stride=1, 
                                                   bias=False),
                                         BatchNorm(256),
                                         nn.ReLU())
```
(4) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-deeplabv3+.py、test-deeplabv3+.py文件到同级目录

## 开始转换

```python
python pytorch2caffe-deeplabv3+.py
```

执行该命令会生成deeplabv3+.caffemodel等文件。

## 结果校验

```python
python test-deeplabv3+.py
```









