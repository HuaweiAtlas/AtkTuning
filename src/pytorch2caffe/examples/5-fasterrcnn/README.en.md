EN|[CN](README.md)
# Converting the Faster-RCNN Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [Faster R-CNN model source code](https://github.com/jwyang/faster-rcnn.pytorch).

(2) Create the `data/pretrained_model/res101/pascal_voc/` directory in the directory where the model source code is located, and store the trained Faster R-CNN model in this directory. For example:

`data/pretrained_model/res101/pascal_voc/faster_rcnn_3_2_625.pth`

`res101` indicates that `backbone` of the network is `resnet101`, and `pascal_voc` indicates that the model is trained using `pascal_voc` dataset.

(3) Modify the source code of the Faster R-CNN model.

- Modify the `forward()` function of the `/lib/model/faster_rcnn/faster_rcnn.py` module.

Before modification:

```python
def forward(self, im_data, im_info, gt_boxes, num_boxes):
    batch_size = im_data.size(0)

    im_info = im_info.data
    gt_boxes = gt_boxes.data
    num_boxes = num_boxes.data
    ...
    rois = Variable(rois)
```

After modification:

```python
def forward(self, im_data, im_info, gt_boxes, num_boxes):
    batch_size = im_data.size(0)

    #im_info = im_info.data
    gt_boxes = gt_boxes.data
    num_boxes = num_boxes.data
    ...
    #rois = Variable(rois)
```

- Modify the `forward()` function of the `/lib/model/rpn/rpn.py` module.

Before modification:

```python
rois = self.RPN_proposal((rpn_cls_prob.data, rpn_bbox_pred.data,im_info, cfg_key))
```

After modification:

```python
rois = self.RPN_proposal((rpn_cls_prob, rpn_bbox_pred,im_info, cfg_key))
```

- Modify the `/lib/model/rpn/proposal_layer.py` module.

1) Add a module.

```python
from torch.autograd import Variable
```

2) Modify the `forward()` function.

After modification:

```python
    def forward(self, input):
        scores = input[0].data[:, self._num_anchors:, :, :]
        bbox_deltas = input[1].data
        im_info = input[2].data
        cfg_key = input[3]

        ...

        # 2. clip predicted boxes to image
        proposals = clip_boxes(proposals, im_info, batch_size)
        # proposals = clip_boxes_batch(proposals, im_info, batch_size)

        # assign the score to 0 if it's non keep.
        keep_idx = self._filter_boxes(proposals, min_size * im_info[:, 2])
        # trim keep index to make it euqal over batch
        keep_idx = torch.cat(tuple(keep_idx), 0)
        trim_size = -1
        scores_keep = scores.view(-1)[keep_idx].view(batch_size, trim_size)
        proposals_keep = proposals.view(-1, 4)[keep_idx, :].contiguous().view(batch_size, trim_size, 4)
        _, order = torch.sort(scores_keep, 1, True)
        '''
        scores_keep = scores
        proposals_keep = proposals
        _, order = torch.sort(scores_keep, 1, True)
        '''
        output = scores.new(batch_size,post_nms_topN , 5).zero_()
        #output = scores.new(batch_size,245, 5).zero_()
		
        ...

        output=Variable(output)    
        return output
```
(4) Copy the Caffe and convert folders to the same directory.

(5) Modify the pytorch_to_caffe.py conversion tool module to support PyTorch custom operator conversion.

- Add the support for _ProposalLayer custom operator.

1) Import the custom operator module.

```python
from model.rpn.proposal_layer import _ProposalLayer
```

2) Add a replacement function for the module.

```python
def _proposal(self,input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_proposal_forward(self,input)
    name = log.add_layer(name='proposal')
    log.add_blobs([x],name='proposal')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=[log.blobs(input[0]),log.blobs(input[1]),
                                          log.blobs(input[2])],top=[log.blobs(x)])
                               
    layer.param.python_param.module = 'rpn.proposal_layer'
    layer.param.python_param.layer = 'ProposalLayer'
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3) Add a replacement operation.

```python
raw_proposal_forward = _ProposalLayer.forward
_ProposalLayer.forward = _proposal
```

- Add the ROIAlign custom operator conversion.

1) Import the custom operator module.

```python
from model.roi_layers import ROIAlign
```

2) Add a replacement function for the module.

```python
def _roi_align(self,input,rois):
    x=raw_align(self,input,rois)
    name = log.add_layer(name='roi_pool')
    log.add_blobs([x],name='roi_pool')
    layer = caffe_net.Layer_param(name=name,type='ROIAlign',
                         bottom=[log.blobs(input),log.blobs(rois)],top=[log.blobs(x)])
    layer.param.roi_align_param.pooled_h=self.output_size[0]
    layer.param.roi_align_param.pooled_w=self.output_size[1]
    layer.param.roi_align_param.spatial_scale=self.spatial_scale
    log.cnet.add_layer(layer)
    return x
```

3) Add a replacement operation.

```python
raw_align = ROIAlign.forward
ROIAlign.forward = _roi_align
```


## Conversion

Copy `test_net.py` and rename it `pytorch2caffe-faster-rcnn.py`, 
After the modification: 

```python
if __name__ == '__main__':
    ...
    pytorch_to_caffe.trans_net(fasterRCNN, [im_data, im_info, gt_boxes, num_boxes], 'fasterrcnn')
    pytorch_to_caffe.save_prototxt('./faster-rcnn.prototxt')
    pytorch_to_caffe.save_caffemodel('./faster-rcnn.caffemodel')
```

Run the following command to convert the model:

```python
python pytorch2caffe-faster-rcnn.py --dataset pascal_voc --net res101 --checksession 3 --checkepoch 2 --checkpoint 625 --load_dir ./data/pretrained_model
```

The values of `checksession, checkepoch, and checkpint` vary depending on the actual training model. After this command is executed, files such as faster-rcnn.caffemodel are generated.

**Model Conversion FAQs**

- An import error occurs, for example:

(1)

```python
ImportError: cannot import name _mask
```

 Solution: Recompile the Python API of the coco by referring to [issues](https://github.com/jwyang/faster-rcnn.pytorch/issues/410), and replace the pycocotools folder in the lib directory with the newly compiled pycocotools folder.

```python
cd data
git clone <https://github.com/pdollar/coco.git> 
cd coco/PythonAPI
make
cd ../../../lib/
rm -rf pycocotools
cp -r ../data/cocoapi-master/PythonAPI/pycocotools/ ./
```

(2)

```python
ImportError: cannot import name _C
```

Solution: See [issues](https://github.com/jwyang/faster-rcnn.pytorch/issues/503).

```python
cd lib
rm -rf lib/build/
python setup.py build
cp build/lib.linux-x86_64-2.7/model/_C.so ./model/
```

- A VOC interface error occurs.

```python
pytorch2caffe-1.1.0/example/5-fasterrcnn/lib/datasets/pascal_voc.py", line 107, in _load_image_set_index
    'Path does not exist: {}'.format(image_set_file)
AssertionError: Path does not exist: /pytorch2caffe-1.1.0/example/5-fasterrcnn/data/VOCdevkit2007/VOC2007/ImageSets/Main/test.txt
```

Solution: Configure PASCAL_VOC dataset by referring to **Data Preparation** in [faster-rcnn model source code](https://github.com/jwyang/faster-rcnn.pytorch), create the soft link `VOCdevkit2007` of the dataset in the data directory. (Note: After the soft link is created, the soft link is `VOCdevkit`. You need to change the soft link name to `VOCdevkit2007`.)

**PASCAL_VOC 07+12**: Please follow the instructions in [py-faster-rcnn](https://github.com/rbgirshick/py-faster-rcnn#beyond-the-demo-installation-for-training-and-testing-models) to prepare VOC datasets. Actually, you can refer to any others. After downloading the data, create softlinks in the folder data/.

## Result Verification

```python
python test-faster-rcnn.py --dataset pascal_voc --net res101 --checksession 3 --checkepoch 2 --checkpoint 625 --load_dir ./data/pretrained_model
```

## Caffe2D conversion of Faster R-CNN

- Modify the inputs of the faster-rcnn.prototxt file.


The pytorch model of Faster R-CNN has four inputs, but only two inputs are useful in the Caffe model. The redundant inputs need to be deleted, and the format of the input layer needs to be modified.

Before modification:

```python
name: "fasterrcnn"
input: "blob1"
input: "blob2"
input: "blob3"
input: "blob4"
input_dim: 1
input_dim: 3
input_dim: 850
input_dim: 600
input_dim: 1
input_dim: 3
input_dim: 1
input_dim: 5
input_dim: 1
```

After modification:

```python
name: "fasterrcnn"
input: "blob1"
input: "blob2"
input_shape {
  dim: 1
  dim:3
  dim:850
  dim:600
}
input_shape {
  dim:1
  dim:3
}
```

- Modify the converted proposal custom operator and add parameters.

The PyTorch2caffe tool converts the custom operator proposal layer of the Faster R-CNN model to the Python layer. When converting the model to the D model, you need to change the custom operator proposal layer to the D proposal operator and add related parameters.

Before modification:

```python
layer {
  name: "proposal1"
  type: "Python"
  bottom: "view2"
  bottom: "conv97"
  bottom: "blob2"
  top: "proposal1"
  python_param {
    module: "rpn.proposal_layer"
    layer: "ProposalLayer"
  }
}
```

After modification:

```python
layer {
  name: "proposal1"
  type: "Proposal"
  bottom: "view2"
  bottom: "conv97"
  bottom: "blob2"
  top: "proposal1"
  proposal_param {
     feat_stride:16
     min_size: 16
     ratio: 0.5
     ratio: 1
     ratio: 2
     scale: 8
     scale: 16
     scale: 32
     pre_nms_topn: 6000
     post_nms_topn:300
     nms_thresh: 0.8
  }
}
```

The post-processing operator FSRDetectionOutput of the D platform has input constraints. Therefore, you need to modify the parameters of the proposal operator by changing the value of the `post_nms_topn` parameter of the proposal operator to a multiple of 16. For example:

```python
post_nms_topn:304
```

- Modify the reshape layer.

The final output cls_prob and bbox_pred of the PyTorch model of Faster R-CNN are reshaped twice. As a result, the data format is changed and the model cannot be directly converted to the D model. The last two reshape layers need to be removed.

Delete the following information:

```python
layer {
  name: "view4"
  type: "Reshape"
  bottom: "softmax2"
  top: "view4"
  reshape_param {
    shape {
      dim: 1
      dim: 300
      dim: -1
    }
  }
}
layer {
  name: "view5"
  type: "Reshape"
  bottom: "fc1"
  top: "view5"
  reshape_param {
    shape {
      dim: 1
      dim: 300
      dim: -1
    }
  }
}
```

- Modify the mean layer.

The PyTorch model of Faster R-CNN contains the mean operation, which corresponds to the reduction operator of Caffe. PyTorch uses the mean operation twice for data. During inference of the converted D model, the address of the mte instruction is out of range. As a result, the model cannot be inferred. In the prototxt file, delete the mean1 layer, and modify the bottom of the mean2 layer.

Before modification:

```python
layer {
  name: "mean1"
  type: "Reduction"
  bottom: "relu101"
  top: "mean1"
  reduction_param {
    operation: MEAN
    axis: 3
  }
}
layer {
  name: "mean2"
  type: "Reduction"
  bottom: "mean1"
  top: "mean2"
  reduction_param {
    operation: MEAN
    axis: 2
  }
}
```

After modification:

```python
layer {
  name: "mean2"
  type: "Reduction"
  bottom: "relu101"
  top: "mean2"
  reduction_param {
    operation: MEAN
    axis: 2
  }
}
```

- Add the post-processing operator of the D platform.

The post-processing operator `FSRDetectionOutput` is developed for the Faster R-CNN model on the D platform. Therefore, the operator can be added to the faster-rcnn.prototxt file when the Caffe model is converted into the D model.

Add the following information to the end of the faster-rcnn.prototxt file:

```python
layer {
  name: "detection_output"
  type: "FSRDetectionOutput"
  bottom: "softmax2"
  bottom: "fc1"
  bottom: "proposal1"
  top: "out_box_num"
  top: "detection_out"
    detection_output_param {
        num_classes: 21
        nms_threshold: 0.3
        confidence_threshold: 0.6
  }
}
```

