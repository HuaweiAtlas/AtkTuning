[EN](README.en.md)|CN
# 将pytorch框架下的faster-rcnn模型转换成caffe模型

[TOC]

## 准备工作

（1）下载[faster-rcnn模型源码](https://github.com/jwyang/faster-rcnn.pytorch)

（2） 在模型源码同级目录下创建`data/pretrained_model/res101/pascal_voc/`目录，并将训练后的faster-rcnn的模型放置在该目录下。例如：

`data/pretrained_model/res101/pascal_voc/faster_rcnn_3_2_625.pth`

其中`res101`表示该网络的`backbone`为`resnet101`,`pascal_voc`表示使用`pascal_voc`数据集训练的该模型。

（3）修改faster-rcnn模型源码的代码

- 修改`/lib/model/faster_rcnn/faster_rcnn.py`模块的`forward()`函数

修改前：

```python
def forward(self, im_data, im_info, gt_boxes, num_boxes):
    batch_size = im_data.size(0)

    im_info = im_info.data
    gt_boxes = gt_boxes.data
    num_boxes = num_boxes.data
    ...
    rois = Variable(rois)
```

修改后：

```python
def forward(self, im_data, im_info, gt_boxes, num_boxes):
    batch_size = im_data.size(0)

    #im_info = im_info.data
    gt_boxes = gt_boxes.data
    num_boxes = num_boxes.data
    ...
    #rois = Variable(rois)
```

- 修改`/lib/model/rpn/rpn.py`模块的`forward()`函数

修改前：

```python
rois = self.RPN_proposal((rpn_cls_prob.data, rpn_bbox_pred.data,im_info, cfg_key))
```

修改后：

```python
rois = self.RPN_proposal((rpn_cls_prob, rpn_bbox_pred,im_info, cfg_key))
```

- 修改`/lib/model/rpn/proposal_layer.py`模块

1）新增模块

```python
from torch.autograd import Variable
```

2）修改`forward()`函数

修改后：

```python
    def forward(self, input):
        scores = input[0].data[:, self._num_anchors:, :, :]
        bbox_deltas = input[1].data
        im_info = input[2].data
        cfg_key = input[3]

        ...

        # 2. clip predicted boxes to image
        proposals = clip_boxes(proposals, im_info, batch_size)
        # proposals = clip_boxes_batch(proposals, im_info, batch_size)

        # assign the score to 0 if it's non keep.
        keep_idx = self._filter_boxes(proposals, min_size * im_info[:, 2])
        # trim keep index to make it euqal over batch
        keep_idx = torch.cat(tuple(keep_idx), 0)
        trim_size = -1
        scores_keep = scores.view(-1)[keep_idx].view(batch_size, trim_size)
        proposals_keep = proposals.view(-1, 4)[keep_idx, :].contiguous().view(batch_size, trim_size, 4)
        _, order = torch.sort(scores_keep, 1, True)
        '''
        scores_keep = scores
        proposals_keep = proposals
        _, order = torch.sort(scores_keep, 1, True)
        '''
        output = scores.new(batch_size,post_nms_topN , 5).zero_()
        #output = scores.new(batch_size,245, 5).zero_()
		
        ...

        output=Variable(output)    
        return output
```
（4） 拷贝工具中的Caffe、convert文件夹到同级目录

（5）修改pytorch_to_caffe.py转换工具模块，增加对pytorch自定义算子转换的支持。

- 增加对_ProposalLayer自定义算子的支持

1）导入该自定义算子模块，如下：

```python
from model.rpn.proposal_layer import _ProposalLayer
```

2）增加该模块的替换函数，如下：

```python
def _proposal(self,input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_proposal_forward(self,input)
    name = log.add_layer(name='proposal')
    log.add_blobs([x],name='proposal')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=[log.blobs(input[0]),log.blobs(input[1]),
                                          log.blobs(input[2])],top=[log.blobs(x)])
                               
    layer.param.python_param.module = 'rpn.proposal_layer'
    layer.param.python_param.layer = 'ProposalLayer'
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

3）增加替换操作，如下:

```python
raw_proposal_forward = _ProposalLayer.forward
_ProposalLayer.forward = _proposal
```

- 增加对ROIAlign自定义算子的转换

1）导入该自定义算子模块，如下：

```python
from model.roi_layers import ROIAlign
```

2）增加该模块的替换函数，如下：

```python
def _roi_align(self,input,rois):
    x=raw_align(self,input,rois)
    name = log.add_layer(name='roi_pool')
    log.add_blobs([x],name='roi_pool')
    layer = caffe_net.Layer_param(name=name,type='ROIAlign',
                         bottom=[log.blobs(input),log.blobs(rois)],top=[log.blobs(x)])
    layer.param.roi_align_param.pooled_h=self.output_size[0]
    layer.param.roi_align_param.pooled_w=self.output_size[1]
    layer.param.roi_align_param.spatial_scale=self.spatial_scale
    log.cnet.add_layer(layer)
    return x
```

3）增加替换操作，如下：

```python
raw_align = ROIAlign.forward
ROIAlign.forward = _roi_align
```


## 开始转换

复制`test_net.py`并重命名为`pytorch2caffe-faster-rcnn.py`,
修改代码后：

```python
if __name__ == '__main__':
    ...
    pytorch_to_caffe.trans_net(fasterRCNN, [im_data, im_info, gt_boxes, num_boxes], 'fasterrcnn')
    pytorch_to_caffe.save_prototxt('./faster-rcnn.prototxt')
    pytorch_to_caffe.save_caffemodel('./faster-rcnn.caffemodel')
```

然后执行下列命令行进行模型转换

```python
python pytorch2caffe-faster-rcnn.py --dataset pascal_voc --net res101 --checksession 3 --checkepoch 2 --checkpoint 625 --load_dir ./data/pretrained_model
```

其中，`checksession、checkepoch、checkpint`的值根据实际训练模型的更改。执行该命令会生成faster-rcnn.caffemodel等文件。

**模型转换FAQ**

- 出现import错误，例如：

（1）

```python
ImportError: cannot import name _mask
```

 解决方式：参考[issus](https://github.com/jwyang/faster-rcnn.pytorch/issues/410)解决方式，重新编译coco的PythonAPI，并将lib目录下的pycocotools文件夹替换成新编译的pycocotools文件夹

```python
cd data
git clone <https://github.com/pdollar/coco.git> 
cd coco/PythonAPI
make
cd ../../../lib/
rm -rf pycocotools
cp -r ../data/cocoapi-master/PythonAPI/pycocotools/ ./
```

（2）

```python
ImportError: cannot import name _C
```

解决方式：参考[issus](https://github.com/jwyang/faster-rcnn.pytorch/issues/503)解决方式

```python
cd lib
rm -rf lib/build/
python setup.py build
cp build/lib.linux-x86_64-2.7/model/_C.so ./model/
```

- 出现VOC接口错误

```python
pytorch2caffe-1.1.0/example/5-fasterrcnn/lib/datasets/pascal_voc.py", line 107, in _load_image_set_index
    'Path does not exist: {}'.format(image_set_file)
AssertionError: Path does not exist: /pytorch2caffe-1.1.0/example/5-fasterrcnn/data/VOCdevkit2007/VOC2007/ImageSets/Main/test.txt
```

解决方式：参考[faster-rcnn模型源码](https://github.com/jwyang/faster-rcnn.pytorch)的**Data Preparation**部分，配置PASCAL_VOC数据集，之后在data目录下创建该数据集的软链接`VOCdevkit2007`(备注：直接创建软链接后，该软链接为`VOCdevkit`，需要修改一下软链接的名称，改为`VOCdevkit2007`)

**PASCAL_VOC 07+12**: Please follow the instructions in [py-faster-rcnn](https://github.com/rbgirshick/py-faster-rcnn#beyond-the-demo-installation-for-training-and-testing-models) to prepare VOC datasets. Actually, you can refer to any others. After downloading the data, create softlinks in the folder data/.

## 结果校验

```python
python test-faster-rcnn.py --dataset pascal_voc --net res101 --checksession 3 --checkepoch 2 --checkpoint 625 --load_dir ./data/pretrained_model
```

## faster-rcnn的caffe2D转换

- 修改faster-rcnn.prototxt文件的输入


faster-rcnn的pytorch模型具有4个输入，在caffe模型中有用的只有两个，需要将多余的输入去掉，并修改输入层的格式。

修改前：

```python
name: "fasterrcnn"
input: "blob1"
input: "blob2"
input: "blob3"
input: "blob4"
input_dim: 1
input_dim: 3
input_dim: 850
input_dim: 600
input_dim: 1
input_dim: 3
input_dim: 1
input_dim: 5
input_dim: 1
```

修改后：

```python
name: "fasterrcnn"
input: "blob1"
input: "blob2"
input_shape {
  dim: 1
  dim:3
  dim:850
  dim:600
}
input_shape {
  dim:1
  dim:3
}
```

- 修改转换的proposal自定义算子并添加参数

对于faster-rcnn模型的自定义算子proposal层，pytorch2caffe工具将其转为python层，在转D模型时需要修改为D的proposal算子，并添加相应参数。

修改前：

```python
layer {
  name: "proposal1"
  type: "Python"
  bottom: "view2"
  bottom: "conv97"
  bottom: "blob2"
  top: "proposal1"
  python_param {
    module: "rpn.proposal_layer"
    layer: "ProposalLayer"
  }
}
```

修改后：

```python
layer {
  name: "proposal1"
  type: "Proposal"
  bottom: "view2"
  bottom: "conv97"
  bottom: "blob2"
  top: "proposal1"
  proposal_param {
     feat_stride:16
     min_size: 16
     ratio: 0.5
     ratio: 1
     ratio: 2
     scale: 8
     scale: 16
     scale: 32
     pre_nms_topn: 6000
     post_nms_topn:300
     nms_thresh: 0.8
  }
}
```

由于D平台后处理算子FSRDetectionOutput存在输入约束，因此需要修改proposal算子的参数，将proposal的`post_nms_topn`的参数修改为16的倍数。例如：

```python
post_nms_topn:304
```

- 修改reshape层

faster-rcnn的pytorch模型最终的输出cls_prob与bbox_pred会经过两次reshape操作，改变数据格式导致无法直接转D模型，需要去掉最后两个reshape层。

删除如下部分：

```python
layer {
  name: "view4"
  type: "Reshape"
  bottom: "softmax2"
  top: "view4"
  reshape_param {
    shape {
      dim: 1
      dim: 300
      dim: -1
    }
  }
}
layer {
  name: "view5"
  type: "Reshape"
  bottom: "fc1"
  top: "view5"
  reshape_param {
    shape {
      dim: 1
      dim: 300
      dim: -1
    }
  }
}
```

- 修改mean层

faster-rcnn的pytorch模型中存在mean操作，对应于caffe的reduction算子，pytorch对数据连续使用了两次mean操作，转成的D模型在进行推理的时候会存在mte指令的地址越界问题，导致模型不能推理。需要修改prototxt文件，删除mean1层，并修改mean2的bottom

修改前：

```python
layer {
  name: "mean1"
  type: "Reduction"
  bottom: "relu101"
  top: "mean1"
  reduction_param {
    operation: MEAN
    axis: 3
  }
}
layer {
  name: "mean2"
  type: "Reduction"
  bottom: "mean1"
  top: "mean2"
  reduction_param {
    operation: MEAN
    axis: 2
  }
}
```

修改后：

```python
layer {
  name: "mean2"
  type: "Reduction"
  bottom: "relu101"
  top: "mean2"
  reduction_param {
    operation: MEAN
    axis: 2
  }
}
```

- 增加D平台的后处理算子

D平台专门为faster-rcnn模型开发了后处理算子`FSRDetectionOutput`，因此在caffe模型转D模型的时候可以添加在faster-rcnn.prototxt中。

在faster-rcnn.prototxt的最后增加以下部分：

```python
layer {
  name: "detection_output"
  type: "FSRDetectionOutput"
  bottom: "softmax2"
  bottom: "fc1"
  bottom: "proposal1"
  top: "out_box_num"
  top: "detection_out"
    detection_output_param {
        num_classes: 21
        nms_threshold: 0.3
        confidence_threshold: 0.6
  }
}
```

