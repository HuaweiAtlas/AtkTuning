[EN](README.en.md)|CN
将pytorch框架下的mtcnn模型转换成caffe模型

[TOC]

## 准备工作

(1) 下载[mtcnn模型源码](https://github.com/Sierkinhane/mtcnn-pytorch)

(2) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-mtcnn-pnet.py、pytorch2caffe-mtcnn-rnet.py、pytorch2caffe-mtcnn-onet.py、test-pnet.py、test-rnet.py、test-onet.py文件到同级目录

## 开始转换

mtcnn模型是由三个模型级联组合而成，包含三个单独的模型，在转换时每个模型独立转换。

```python
python pytorch2caffe-mtcnn-pnet.py
python pytorch2caffe-mtcnn-rnet.py
python pytorch2caffe-mtcnn-onet.py
```

执行该命令会生成pnet.caffemodel、rnet.caffemodel、onet.caffemodel等文件。

**模型转换FAQ**

- 未识别字符，例如：

```python
SyntaxError: Non-ASCII character '\xc3' in file/10-mtcnn/mtcnn/core/detect.py 
```

解决方式：在detect.py模型添加下列代码。

```python
#coding=UTF-8
```

## 结果校验

```python
python test-pnet.py
python test-rnet.py
python test-onet.py
```









