import torch
from convert import pytorch_to_caffe
from mtcnn.core.models import ONet
from torch.autograd import Variable

onet = ONet(use_cuda=False)
onet.load_state_dict(torch.load('./model_store/onet_epoch.pt',
                     map_location=lambda storage, loc: storage))
onet.eval()
input_var = Variable(torch.rand([1, 3, 48, 48]))
print('****************start pytorch2caffe****************')
pytorch_to_caffe.trans_net(onet, [input_var], 'onet')
pytorch_to_caffe.save_prototxt('onet.prototxt')
pytorch_to_caffe.save_caffemodel('onet.caffemodel')
print('***********************done************************')
