import torch
from convert import pytorch_to_caffe
from mtcnn.core.models import PNet
from torch.autograd import Variable


pnet = PNet(use_cuda=False)
pnet.load_state_dict(torch.load('./model_store/pnet_epoch.pt',
                     map_location=lambda storage, loc: storage))
pnet.eval()
input_var = Variable(torch.rand([1, 3, 12, 12]))
print('******************start pytorch2caffe******************')
pytorch_to_caffe.trans_net(pnet, [input_var], 'pnet')
pytorch_to_caffe.save_prototxt('pnet.prototxt')
pytorch_to_caffe.save_caffemodel('pnet.caffemodel')
print('*************************done********************')
