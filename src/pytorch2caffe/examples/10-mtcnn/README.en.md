EN|[CN](README.md)
Converting the MTCNN Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [MTCNN model source code](https://github.com/Sierkinhane/mtcnn-pytorch).

(2) Copy the Caffe and convert folders and the pytorch2caffe-mtcnn-pnet.py, pytorch2caffe-mtcnn-rnet.py, pytorch2caffe-mtcnn-onet.py, test-pnet.py, test-rnet, and test-onet.py files to the same directory.

## Conversion

The MTCNN model consists of three cascaded models. Each model is converted independently.

```python
python pytorch2caffe-mtcnn-pnet.py
python pytorch2caffe-mtcnn-rnet.py
python pytorch2caffe-mtcnn-onet.py
```

After this command is executed, files such as pnet.caffemodel, rnet.caffemodel, and onet.caffemodel are generated.

**Model Conversion FAQs**

- There are unrecognized characters, for example:

```python
SyntaxError: Non-ASCII character '\xc3' in file/10-mtcnn/mtcnn/core/detect.py 
```

Solution: Add the following code to the detect.py model:

```python
#coding=UTF-8
```

## Result Verification

```python
python test-pnet.py
python test-rnet.py
python test-onet.py
```









