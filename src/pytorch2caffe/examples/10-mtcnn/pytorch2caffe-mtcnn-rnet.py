import torch
from convert import pytorch_to_caffe
from mtcnn.core.models import RNet
from torch.autograd import Variable

rnet = RNet(use_cuda=False)
rnet.load_state_dict(torch.load('./model_store/rnet_epoch.pt',
                     map_location=lambda storage, loc: storage))
rnet.eval()
input_var = Variable(torch.rand([1, 3, 24, 24]))
print('********************start pytorch2caffe*****************')
pytorch_to_caffe.trans_net(rnet, [input_var], 'rnet')
pytorch_to_caffe.save_prototxt('rnet.prototxt')
pytorch_to_caffe.save_caffemodel('rnet.caffemodel')
print('**************************done**************************')
