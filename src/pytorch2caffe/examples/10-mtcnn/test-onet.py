import torch
from mtcnn.core.models import ONet
from torch.autograd import Variable
import numpy as np
import caffe


input_size = [1, 3, 48, 48]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

o_model_path = "./model_store/onet_epoch.pt"
onet = ONet(use_cuda=False)
onet.load_state_dict(torch.load('./model_store/onet_epoch.pt',
                     map_location=lambda storage, loc: storage))
onet.eval()
input_var = Variable(torch.from_numpy(input_data))
det, box, landmark = onet(input_var)

caffe_model = './onet.prototxt'
caffe_weights = './onet.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()

diff_sigmoid1 = np.abs(det.data.cpu().numpy().flatten() - caffe_out['sigmoid1'].flatten())
diff_fc3 = np.abs(box.data.cpu().numpy().flatten() - caffe_out['fc3'].flatten())
diff_fc4 = np.abs(landmark.data.cpu().numpy().flatten() - caffe_out['fc4'].flatten())

print('diff_sigmoid1:')
print('diff_max:', diff_sigmoid1.max(), '    ', 'diff_mean:', diff_sigmoid1.mean(), '    ',
      'diff_min:', diff_sigmoid1.min(), '    ', 'diff_median:', np.median(diff_sigmoid1))

print('diff_fc3:')
print('diff_max:', diff_fc3.max(), '    ', 'diff_mean:', diff_fc3.mean(), '    ',
      'diff_min:', diff_fc3.min(), '    ', 'diff_median:', np.median(diff_fc3))

print('diff_fc4:')
print('diff_max:', diff_fc4.max(), '    ', 'diff_mean:', diff_fc4.mean(), '    ',
      'diff_min:', diff_fc4.min(), '    ', 'diff_median:', np.median(diff_fc4))
