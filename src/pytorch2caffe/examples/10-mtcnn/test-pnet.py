import torch
from mtcnn.core.models import PNet
from torch.autograd import Variable
import numpy as np
import caffe


input_size = [1, 3, 12, 12]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

pnet = PNet(use_cuda=False)
pnet.load_state_dict(torch.load('./model_store/pnet_epoch.pt',
                     map_location=lambda storage, loc: storage))
pnet.eval()
input_var = Variable(torch.from_numpy(input_data))
label, offset = pnet(input_var)

caffe_model = './pnet.prototxt'
caffe_weights = './pnet.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()
diff_sigmoid1 = np.abs(label.data.cpu().numpy().flatten()
                       - caffe_out['sigmoid1'].flatten())
diff_conv5 = np.abs(offset.data.cpu().numpy().flatten()
                    - caffe_out['conv5'].flatten())

print('diff_sigmoid1:')
print('diff_max:', diff_sigmoid1.max(), '    ', 'diff_mean:', diff_sigmoid1.mean(), '    ',
      'diff_min:', diff_sigmoid1.min(), '    ', 'diff_median:', np.median(diff_sigmoid1))

print('diff_conv5:')
print('diff_max:', diff_conv5.max(), '    ', 'diff_mean:', diff_conv5.mean(), '    ',
      'diff_min:', diff_conv5.min(), '    ', 'diff_median:', np.median(diff_conv5))
