import torch
from darknet import Darknet
from torch.autograd import Variable
from convert import pytorch_to_caffe


net = Darknet('./cfg/yolo_v3.cfg')
net.load_weights('./yolov3.weights')
net.eval()

input_var = Variable(torch.rand([1, 3, 416, 416]))

print('*********start pytorch2caffe************')
pytorch_to_caffe.trans_net(net, [input_var], 'yolov3')
pytorch_to_caffe.save_prototxt('yolov3.prototxt')
pytorch_to_caffe.save_caffemodel('yolov3.caffemodel')

print('*********done***************************')
