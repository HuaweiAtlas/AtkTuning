[EN](README.en.md)|CN
将pytorch框架下的yolov3模型转换成caffe模型

[TOC]

## 准备工作

(1) 下载[yolov3模型源码](https://github.com/andy-yun/pytorch-0.4-yolov3)

(2)  下载训练好的yolov3模型，并放置在同级目录下

[yolov3模型](https://pjreddie.com/media/files/yolov3.weights)如果无法下载，则自行训练一个模型放置在同级目录下。

(3) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-yolov3.py、test-yolov3.py文件到同级目录

(4) 修改pytorch_to_caffe.py转换工具模块，增加对darknet.py目录下自定义Upsample函数的转换的支持

- 导入Upsample函数

```python
from darknet import Upsample
```

- 增加替换函数

```python
def _upsample(self, input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_Upsample_forward(self,input)
    bottom_blobs=[log.blobs(input)]
    name = log.add_layer(name='upsample')
    top_blobs = log.add_blobs([x], name='upsample')
    layer = caffe_net.Layer_param(name=name, type='Upsample',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.upsample_param.scale = self.stride
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

- 增加替换操作

```python
raw_Upsample_forward = Upsample.forward
Upsample.forward = _upsample
```



## 开始转换

```python
python pytorch2caffe-yolov3.py
```

执行该命令会生成yolov3.caffemodel等文件

**模型转换FAQ**

- 出现python语法错误，例如：

```python
pytorch2caffe-1.1.0/example/4-yolov3/utils.py", line 472
    print('%s %s' % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), message), file=f)
                                                                                         ^
SyntaxError: invalid syntax

```

 解决方式：将相应代码注释掉

```python
def savelog(message):
    logging(message)
    '''
    with open('savelog.txt', 'a') as f:
        print('%s %s' % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), message), file=f)
    '''
```



## 结果校验

（1）修改utils.py模块中的`do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True)`函数

修改前：

```python
def do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True):
    model.eval()
    t0 = time.time()
    img = image2torch(img)
    t1 = time.time()

    img = img.to(torch.device("cuda" if use_cuda else "cpu"))
    t2 = time.time()

    out_boxes = model(img)
    if model.net_name() == 'region': # region_layer
        shape=(0,0)
    else:
        shape=(model.width, model.height)
    boxes = get_all_boxes(out_boxes, shape, conf_thresh, model.num_classes,         	                           use_cuda=use_cuda)[0]

    t3 = time.time()
    boxes = nms(boxes, nms_thresh)
    t4 = time.time()

    if False:
        print('-----------------------------------')
        print(' image to tensor : %f' % (t1 - t0))
        print('  tensor to cuda : %f' % (t2 - t1))
        print('         predict : %f' % (t3 - t2))
        print('             nms : %f' % (t4 - t3))
        print('           total : %f' % (t4 - t0))
        print('-----------------------------------')
    return boxes
```

修改后：

```python
def do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True):
    model.eval()
    t0 = time.time()
    img = image2torch(img)
    input = img
    t1 = time.time()

    img = img.to(torch.device("cuda" if use_cuda else "cpu"))
    t2 = time.time()

    out_boxes,out1,out2,out3 = model(img)
    if model.net_name() == 'region': # region_layer
        shape=(0,0)
    else:
        shape=(model.width, model.height)
    boxes = get_all_boxes(out_boxes, shape, conf_thresh, model.num_classes,      	    	                      use_cuda=use_cuda)[0]

    t3 = time.time()
    boxes = nms(boxes, nms_thresh)
    t4 = time.time()

    if False:
        print('-----------------------------------')
        print(' image to tensor : %f' % (t1 - t0))
        print('  tensor to cuda : %f' % (t2 - t1))
        print('         predict : %f' % (t3 - t2))
        print('             nms : %f' % (t4 - t3))
        print('           total : %f' % (t4 - t0))
        print('-----------------------------------')
    return boxes,out1,out2,out3,input
```

（2）修改darknet.py模型中的`class Darknet(nn.Module)`类的`forward()`函数的返回值

修改前：

```python
return x if outno == 0 else out_boxes
```

修改后：

```python
return x if outno == 0 else out_boxes,outputs[81],outputs[93],outputs[105]
```

（3）执行校验脚本

```python
python test-yolov3.py
```





