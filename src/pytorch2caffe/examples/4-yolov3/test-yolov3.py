from PIL import Image
import utils
from image import letterbox_image
from darknet import Darknet
import numpy as np
import torch
import caffe

m = Darknet('./cfg/yolo_v3.cfg')
m.print_network()
m.load_weights('./yolov3.weights')

use_cuda = torch.cuda.is_available()
if use_cuda:
    m.cuda()

img = Image.open('./data/dog.jpg').convert('RGB')
sized = letterbox_image(img, m.width, m.height)
boxes, out1, out2, out3, input_data = utils.do_detect(m, sized, 0.5, 0.4, use_cuda)

caffe_model = './yolov3.prototxt'
caffe_weights = './yolov3.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data.data.cpu().numpy()
caffe_out = caffe_net.forward()

diff_conv59 = np.abs(out1.data.cpu().numpy().flatten() - caffe_out['conv59'].flatten())
diff_conv67 = np.abs(out2.data.cpu().numpy().flatten() - caffe_out['conv67'].flatten())
diff_conv75 = np.abs(out3.data.cpu().numpy().flatten() - caffe_out['conv75'].flatten())

print('diff_conv59:')
print('diff_max:', diff_conv59.max(), '    ', 'diff_mean:', diff_conv59.mean(), '    ',
      'diff_min:', diff_conv59.min(), '    ', 'diff_median:', np.median(diff_conv59))

print('diff_conv67:')
print('diff_max:', diff_conv67.max(), '    ', 'diff_mean:', diff_conv67.mean(), '    ',
      'diff_min:', diff_conv67.min(), '    ', 'diff_median:', np.median(diff_conv67))

print('diff_conv75:')
print('diff_max:', diff_conv75.max(), '    ', 'diff_mean:', diff_conv75.mean(), '    ',
      'diff_min:', diff_conv75.min(), '    ', 'diff_median:', np.median(diff_conv75))
