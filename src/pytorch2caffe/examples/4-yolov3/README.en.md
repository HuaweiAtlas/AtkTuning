EN|[CN](README.md)
Converting the YOLOv3 Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [YOLOv3 model source code](https://github.com/andy-yun/pytorch-0.4-yolov3).

(2) Download the trained YOLOv3 model and place it in the same directory.

If the [YOLOv3 model](https://pjreddie.com/media/files/yolov3.weights) cannot be downloaded, train a model and save it in the same directory.

(3) Copy the Caffe and convert folders and the pytorch2caffe-yolov3.py and test-yolov3.py files to the same directory.

(4) Modify the pytorch_to_caffe.py conversion tool module to support the conversion of the customized Upsample function in the darknet.py directory.

- Import the Upsample function.

```python
from darknet import Upsample
```

- Add a replacement function.

```python
def _upsample(self, input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_Upsample_forward(self,input)
    bottom_blobs=[log.blobs(input)]
    name = log.add_layer(name='upsample')
    top_blobs = log.add_blobs([x], name='upsample')
    layer = caffe_net.Layer_param(name=name, type='Upsample',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.upsample_param.scale = self.stride
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

- Add a replacement operation.

```python
raw_Upsample_forward = Upsample.forward
Upsample.forward = _upsample
```



## Conversion

```python
python pytorch2caffe-yolov3.py
```

After this command is executed, files such as yolov3.caffemodel are generated.

**Model Conversion FAQs**

- A Python syntax error occurs, as shown in the following:

```python
pytorch2caffe-1.1.0/example/4-yolov3/utils.py", line 472
    print('%s %s' % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), message), file=f)
                                                                                         ^
SyntaxError: invalid syntax

```

 Solution: Comment out the corresponding code.

```python
def savelog(message):
    logging(message)
    '''
    with open('savelog.txt', 'a') as f:
        print('%s %s' % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), message), file=f)
    '''
```



## Result Verification

(1) Modify the `do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True)` function in the utils.py module.

Before modification:

```python
def do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True):
    model.eval()
    t0 = time.time()
    img = image2torch(img)
    t1 = time.time()

    img = img.to(torch.device("cuda" if use_cuda else "cpu"))
    t2 = time.time()

    out_boxes = model(img)
    if model.net_name() == 'region': # region_layer
        shape=(0,0)
    else:
        shape=(model.width, model.height)
    boxes = get_all_boxes(out_boxes, shape, conf_thresh, model.num_classes,         	                           use_cuda=use_cuda)[0]

    t3 = time.time()
    boxes = nms(boxes, nms_thresh)
    t4 = time.time()

    if False:
        print('-----------------------------------')
        print(' image to tensor : %f' % (t1 - t0))
        print('  tensor to cuda : %f' % (t2 - t1))
        print('         predict : %f' % (t3 - t2))
        print('             nms : %f' % (t4 - t3))
        print('           total : %f' % (t4 - t0))
        print('-----------------------------------')
    return boxes
```

After modification:

```python
def do_detect(model, img, conf_thresh, nms_thresh, use_cuda=True):
    model.eval()
    t0 = time.time()
    img = image2torch(img)
    input = img
    t1 = time.time()

    img = img.to(torch.device("cuda" if use_cuda else "cpu"))
    t2 = time.time()

    out_boxes,out1,out2,out3 = model(img)
    if model.net_name() == 'region': # region_layer
        shape=(0,0)
    else:
        shape=(model.width, model.height)
    boxes = get_all_boxes(out_boxes, shape, conf_thresh, model.num_classes,      	    	                      use_cuda=use_cuda)[0]

    t3 = time.time()
    boxes = nms(boxes, nms_thresh)
    t4 = time.time()

    if False:
        print('-----------------------------------')
        print(' image to tensor : %f' % (t1 - t0))
        print('  tensor to cuda : %f' % (t2 - t1))
        print('         predict : %f' % (t3 - t2))
        print('             nms : %f' % (t4 - t3))
        print('           total : %f' % (t4 - t0))
        print('-----------------------------------')
    return boxes,out1,out2,out3,input
```

(2) Modify the return value of the `forward()` function of `class Darknet(nn.Module)` in the darknet.py model.

Before modification:

```python
return x if outno == 0 else out_boxes
```

After modification:

```python
return x if outno == 0 else out_boxes,outputs[81],outputs[93],outputs[105]
```

(3) Run the verification script.

```python
python test-yolov3.py
```





