import torch
from models.FaceNet import FaceNet
from torch.autograd import Variable
from convert import pytorch_to_caffe


net = FaceNet()
net.load_state_dict(torch.load('./models/facenet.pth'))
net.eval()

img_var = Variable(torch.rand([1, 3, 368, 368]))
print('******************star pytorch2caffe******************')
pytorch_to_caffe.trans_net(net, [img_var], 'facenet')
pytorch_to_caffe.save_prototxt('facenet.prototxt')
pytorch_to_caffe.save_caffemodel('facenet.caffemodel')
print('************************done***************************')
