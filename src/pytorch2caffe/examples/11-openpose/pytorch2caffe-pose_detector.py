import torch
from models.CocoPoseNet import CocoPoseNet
from torch.autograd import Variable
from convert import pytorch_to_caffe


net = CocoPoseNet()
net.load_state_dict(torch.load('./models/posenet.pth'))
net.eval()

img_var = Variable(torch.rand([1, 3, 368, 368]))
print('***************start pytorch2caffe********************')
pytorch_to_caffe.trans_net(net, [img_var], 'posenet')
pytorch_to_caffe.save_prototxt('posenet.prototxt')
pytorch_to_caffe.save_caffemodel('posenet.caffemodel')
print('*************************done*************************')
