[EN](README.en.md)|CN
# 将pytorch框架下的openpose网络转换成caffe模型

[TOC]

## 准备工作

（1）下载[openpose模型源码](https://github.com/TreB1eN/Pytorch0.4.1_Openpose)

（2）根据模型源码README提示，下载相应的模型文件到models文件夹下。如果无法下载可训练对应模型后进行转换。

（3）修改`models`目录下的`FaceNet.py、HandNet.py`文件，将模块中的__`call`__函数重命名为`forward`函数

修改前：

```python
  def __call__(self, x):
```

修改后：

```python
  def forward(self, x):
```

（4)  拷贝工具中的Caffe、convert文件夹及pytorch2caffe-face_detector.py、pytorch2caffe-hand_detector.py、pytorch2caffe-pose_detector.py、test-facenet.py、test-handnet.py、test-posenet.py文件到同级目录

## 开始转换

`openpose`有三种应用场景，分为face_detector、hand_detector、pose_detector,针对三种场景分别进行模型转换。

```python
python pytorch2caffe-face_detector.py
python pytorch2caffe-hand_detector.py 
python pytorch2caffe-pose_detector.py
```



**模型转换FAQ**

- 字符无法识别，例如：

```python
SyntaxError: Non-ASCII character '\xe9' in file /pytorch2caffe-1.1.0/example/11-openpose/entity.py
```

解决方式：在entity.py模块添加下列代码：

```python
#coding=UTF-8
```

- import error，例如：

```python
ImportError: No module named models.CocoPoseNet
```

解决方式：在models目录下新建__init__.py文件，内容可为空。



## 结果校验

```python
python test-facenet.py 
python test-handnet.py
python test-posenet.py
```
