import torch
from torch.autograd import Variable
from models.HandNet import HandNet
from convert import pytorch_to_caffe


net = HandNet()
net.load_state_dict(torch.load('./models/handnet.pth'))
net.eval()

img_var = Variable(torch.rand([1, 3, 368, 368]))
print('************start pytorch2caffe************')
pytorch_to_caffe.trans_net(net, [img_var], 'handnet')
pytorch_to_caffe.save_prototxt('handnet.prototxt')
pytorch_to_caffe.save_caffemodel('handnet.caffemodel')
print('*******************done********************')
