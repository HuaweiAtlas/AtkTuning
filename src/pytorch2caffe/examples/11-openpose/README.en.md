EN|[CN](README.md)
# Converting the OpenPose Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [OpenPose model source code](https://github.com/TreB1eN/Pytorch0.4.1_Openpose).

(2) Download the model file to the models folder by following the README file of the model source code. If the model cannot be downloaded, train the model and convert it.

(3) Modify the `FaceNet.py and HandNet.py` files in the `models` directory and rename the __`call`__ function in the module as the `forward` function.

Before modification:

```python
  def __call__(self, x):
```

After modification:

```python
  def forward(self, x):
```

(4) Copy the Caffe and convert folders and the pytorch2caffe-face_detector.py, pytorch2caffe-hand_detector.py, pytorch2caffe-pose_detector.py, test-facenet.py, test-handnet.py, and test-posenet.py to the same directory.

## Conversion

`openpose` has three application scenarios: face_detector, hand_detector, and pose_detector. Model conversion is performed for the three scenarios.

```python
python pytorch2caffe-face_detector.py
python pytorch2caffe-hand_detector.py 
python pytorch2caffe-pose_detector.py
```



**Model Conversion FAQs**

- The characters cannot be identified, for example:

```python
SyntaxError: Non-ASCII character '\xe9' in file /pytorch2caffe-1.1.0/example/11-openpose/entity.py
```

Solution: Add the following code to the entity.py module:

```python
#coding=UTF-8
```

- Import error, for example:

```python
ImportError: No module named models.CocoPoseNet
```

Solution: Create the __init__.py file in the models directory. The file content can be empty.



## Result Verification

```python
python test-facenet.py 
python test-handnet.py
python test-posenet.py
```
