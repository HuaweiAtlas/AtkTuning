import torch
import numpy as np
from models.CocoPoseNet import CocoPoseNet
from torch.autograd import Variable
import caffe

input_size = [1, 3, 368, 368]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

model_path = "./models/posenet.pth"
net = CocoPoseNet()
net.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
net.eval()
input_var = Variable(torch.from_numpy(input_data))
pafs, heatmaps = net(input_var)

caffe_model = './posenet.prototxt'
caffe_weights = './posenet.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()

diff_conv85 = np.abs(pafs[-1].data.cpu().numpy().flatten()
                     - caffe_out['conv85'].flatten())
diff_conv92 = np.abs(heatmaps[-1].data.cpu().numpy().flatten()
                     - caffe_out['conv92'].flatten())

print('diff_conv85:')
print('diff_max:', diff_conv85.max(), '    ', 'diff_mean:', diff_conv85.mean(), '    ',
      'diff_min:', diff_conv85.min(), '    ', 'diff_median:', np.median(diff_conv85))

print('diff_conv92:')
print('diff_max:', diff_conv92.max(), '    ', 'diff_mean:', diff_conv92.mean(), '    ',
      'diff_min:', diff_conv92.min(), '    ', 'diff_median:', np.median(diff_conv92))
