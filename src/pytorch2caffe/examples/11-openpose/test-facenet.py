import numpy as np
import torch
from models.FaceNet import FaceNet
from torch.autograd import Variable
import caffe

input_size = [1, 3, 368, 368]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

net = FaceNet()
net.load_state_dict(torch.load('./models/facenet.pth'))
net.eval()
input_var = Variable(torch.from_numpy(input_data))
pytorch_output = net(input_var)

caffe_model = './facenet.prototxt'
caffe_weights = './facenet.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()
diff_conv52 = np.abs(pytorch_output[-1].data.cpu().numpy().flatten()
                     - caffe_out['conv52'].flatten())

print('diff_conv52:')
print('diff_max:', diff_conv52.max(), '    ', 'diff_mean:', diff_conv52.mean(), '    ',
      'diff_min:', diff_conv52.min(), '    ', 'diff_median:', np.median(diff_conv52))
