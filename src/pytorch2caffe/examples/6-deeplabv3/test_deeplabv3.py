import numpy as np
import caffe
from torch.autograd import Variable
import deeplab
import torch


input_size = [1, 3, 513, 513]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

model = getattr(deeplab, 'resnet101')(pretrained=True, num_classes=21,
                                      num_groups=None, weight_std=False, beta=False)
checkpoint = torch.load('./deeplab_resnet101_pascal_v3_bn_lr7e-3_epoch50.pth',
                        map_location='cpu')
state_dict = {k[7:]: v for k, v in checkpoint['state_dict'].items() if 'tracked' not in k}
model.load_state_dict(state_dict)
model.eval()
input_var = Variable(torch.from_numpy(input_data))
pytorch_output = model(input_var)

caffe_model = './deeplabv3.prototxt'
caffe_weights = './deeplabv3.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_output = caffe_net.forward()

diff_upsample2 = np.abs(pytorch_output.data.cpu().numpy().flatten()
                        - caffe_output['upsample2'].flatten())
print('diff:')
print('diff_max:', diff_upsample2.max(), '    ', 'diff_mean:', diff_upsample2.mean(), '    ',
      'diff_min:', diff_upsample2.min(), '    ', 'diff_median:', np.median(diff_upsample2))
