import torch
from torch.autograd import Variable
import deeplab
from convert import pytorch_to_caffe


model = getattr(deeplab, 'resnet101')(pretrained=True, num_classes=21,
                                      num_groups=None, weight_std=False, beta=False)
checkpoint = torch.load('./deeplab_resnet101_pascal_v3_bn_lr7e-3_epoch50.pth', map_location='cpu')
state_dict = {k[7:]: v for k, v in checkpoint['state_dict'].items() if 'tracked' not in k}
model.load_state_dict(state_dict)
model.eval()

input_var = Variable(torch.rand([1, 3, 513, 513]))
print('*************start pytorch2caffe****************')
pytorch_to_caffe.trans_net(model, [input_var], 'deeplabv3')
pytorch_to_caffe.save_prototxt('./deeplabv3.prototxt')
pytorch_to_caffe.save_caffemodel('./deeplabv3.caffemodel')
print('*********************done***********************')
