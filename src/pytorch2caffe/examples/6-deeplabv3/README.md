[EN](README.en.md)|CN
将pytorch框架下的deeplabv3模型转换成caffe模型

[TOC]

## 准备工作

(1) 下载[deeplabv3模型源码](https://github.com/chenxi116/DeepLabv3.pytorch)

(2)  根据deeplabv3源码的README，下载训练好的deeplabv3模型，并放置在同级目录下。[deeplabv3模型地址](https://cs.jhu.edu/~cxliu/data/deeplab_resnet101_pascal_v3_bn_lr7e-3_epoch50.pth)

如果模型无法下载，则需要训练一个deeplabv3模型放置在同级目录。

(3) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-deeplabv3.py、test-deeplabv3.py文件到同级目录

## 开始转换

```python
python pytorch2caffe-deeplabv3.py
```

执行该命令会生成deeplabv3.caffemodel等文件。

## 结果校验

```python
python test-deeplabv3.py
```









