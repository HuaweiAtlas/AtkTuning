EN|[CN](README.md)
Converting the DeepLabv3 Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [DeepLabv3 model source code](https://github.com/chenxi116/DeepLabv3.pytorch).

(2) Download the trained DeepLabv3 model and store the model in the same directory by following the README file of the deeplabv3 source code. [DeepLabv3 model address](https://cs.jhu.edu/~cxliu/data/deeplab_resnet101_pascal_v3_bn_lr7e-3_epoch50.pth)

If the model cannot be downloaded, train a DeepLabv3 model and place it in the same directory.

(3) Copy the Caffe and convert folders and the pytorch2caffe-deeplabv3.py and test-deeplabv3.py files to the same directory.

## Conversion

```python
python pytorch2caffe-deeplabv3.py
```

After this command is executed, files such as deeplabv3.caffemodel are generated.

## Result Verification

```python
python test-deeplabv3.py
```









