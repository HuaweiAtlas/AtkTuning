from convert import pytorch_to_caffe
import torch
from torch.autograd import Variable

net = torch.load('./checkpoints/fcn_model_0.pt')
net.eval()

input_var = Variable(torch.randn(1, 3, 160, 160))

print('****************start pytorch2caffe *******************')
pytorch_to_caffe.trans_net(net, [input_var], 'FCN')
pytorch_to_caffe.save_prototxt('./fcn.prototxt')
pytorch_to_caffe.save_caffemodel('./fcn.caffemodel')
print('******************done*********************************')
