[EN](README.en.md)|CN
将pytorch框架下的FCN网络转换成caffe模型

[TOC]

## 准备工作

(1) 下载[FCN模型源码](https://github.com/bat67/pytorch-FCN-easiest-demo)，并根据原项目README训练FCN模型，得到训练结果文件。

修改FCN.py模块的ConvTranspose2d部分。

pytorch的ConvTranspose2d算子具有output_padding参数,caffe不支持。为保证pytorch与caffe经过反卷积之后结果一致，可将pytorch的ConvTranspose2d算子中output_padding置为0，kernel_size加1。

修改前：

```python
self.deconv1 = nn.ConvTranspose2d(512, 512, kernel_size=3, stride=2, padding=1,     		                              dilation=1, output_padding=1)
```

修改后：

```python
self.deconv1 = nn.ConvTranspose2d(512, 512, kernel_size=4, stride=2, padding=1, 		 							       dilation=1, output_padding=0)
```

(2)  进行FCN模型训练，训练脚本为：

```python
python train.py
```

**模型训练FAQ：**

- 出现无法识别字符，例如：

```python
SyntaxError: Non-ASCII character '\xe5' in file...FCN.py
```

解决方式，在FCN.py模块中添加如下代码：

```python
#coding=UTF-8
```

- FCN.py模块出现语法问题

```python
 super().__init__(make_layers(cfg[model]))
TypeError: super() takes at least 1 argument (0 given)
```

解决方式，可在相应位置参考如下方式进行修改：

修改前：

```python
class VGGNet(VGG):
    def __init__(self, pretrained=True, model='vgg16', requires_grad=True,             					 remove_fc=True, show_params=False):
        super().__init__(make_layers(cfg[model]))
        self.ranges = ranges[model]     
```

修改后：

```python
class VGGNet(VGG):
    def __init__(self, pretrained=True, model='vgg16', requires_grad=True,             					 remove_fc=True, show_params=False):
        super(VGGNet,self).__init__(make_layers(cfg[model]))
        self.ranges = ranges[model]
```

- 如果出现visdom问题，例如：

```python
ConnectionError: HTTPConnectionPool(host='localhost', port=8097): Max retries exceeded with url: /events (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f4988150990>: Failed to establish a new connection: [Errno 111] Connection refused',))
```

解决方式，可将train.py中visdom相关代码都注释掉

```python
#import visdom

#   vis = visdom.Visdom()

                
                if np.mod(index, 15) == 0:
                    print(r'Testing... Open http://localhost:8097/ to see test result.')
                    # vis.close()
                    '''
                    vis.images(output_np[:, None, :, :], win='test_pred', opts=dict(title='test prediction')) 
                    vis.images(bag_msk_np[:, None, :, :], win='test_label', opts=dict(title='label'))
                    vis.line(all_test_iter_loss, win='test_iter_loss', opts=dict(title='test iter loss'))
                    
                    '''
```

该训练脚本默认对模型训练100个epoch，可根据实际需要进行更改

```python
if __name__ == "__main__":

    train(epo_num=100, show_vgg_params=False)
```



(3) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-fcn.py、test-fcn.py文件到同级目录

## 开始转换

```python
python pytorch2caffe-fcn.py
```

执行该命令会生成fcn.caffemodel等文件

## 结果校验

```python
python test-fcn.py
```
