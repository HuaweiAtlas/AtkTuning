import torch
import caffe
from torch.autograd import Variable
import numpy as np


net = torch.load('./checkpoints/fcn_model_0.pt')
net.eval()

input_size = [1, 3, 160, 160]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

input_var = Variable(torch.from_numpy(input_data))
pytorch_out = net.forward(input_var)

model = './fcn.prototxt'
weights = './fcn.caffemodel'
caffe_net = caffe.Net(model, weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
out = caffe_net.forward()

print('******diff******')
diff_conv14 = np.abs(pytorch_out.data.cpu().numpy().flatten() - out['conv14'].flatten())
print('diff:')
print('diff_max:', diff_conv14.max(), '    ', 'diff_mean:', diff_conv14.mean(), '    ',
      'diff_min:', diff_conv14.min(), '    ', 'diff_median:', np.median(diff_conv14))
