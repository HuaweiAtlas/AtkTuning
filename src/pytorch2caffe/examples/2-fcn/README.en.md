EN|[CN](README.md)
Converting the FCN  Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [FCN model source code](https://github.com/bat67/pytorch-FCN-easiest-demo), train the FCN model based on the README of the original project, and obtain the training result file.
Modify the ConvTranspose2d part of the FCN.py module.

The ConvTranspose2d operator of PyTorch has the output_padding parameter, which is not supported by Caffe. To ensure that the deconvolution results of PyTorch and Caffe are consistent, set output_padding of the ConvTranspose2d operator in PyTorch to 0 and increase kernel_size by 1.

Before modification:

```python
self.deconv1 = nn.ConvTranspose2d(512, 512, kernel_size=3, stride=2, padding=1,     		                              dilation=1, output_padding=1)
```

After modification:

```python
self.deconv1 = nn.ConvTranspose2d(512, 512, kernel_size=4, stride=2, padding=1, 		 							       dilation=1, output_padding=0)
```

(2) Perform FCN model training. The training script is as follows:

```python
python train.py
```

**Model training FAQs:**

- There are unrecognized characters, for example:

```python
SyntaxError: Non-ASCII character '\xe5' in file...FCN.py
```

Solution: Add the following code to the FCN.py module:

```python
#coding=UTF-8
```

- A syntax error occurs in the FCN.py module.

```python
 super().__init__(make_layers(cfg[model]))
TypeError: super() takes at least 1 argument (0 given)
```

Solution: Modify the code.

Before modification:

```python
class VGGNet(VGG):
    def __init__(self, pretrained=True, model='vgg16', requires_grad=True,             					 remove_fc=True, show_params=False):
        super().__init__(make_layers(cfg[model]))
        self.ranges = ranges[model]     
```

After modification:

```python
class VGGNet(VGG):
    def __init__(self, pretrained=True, model='vgg16', requires_grad=True,             					 remove_fc=True, show_params=False):
        super(VGGNet,self).__init__(make_layers(cfg[model]))
        self.ranges = ranges[model]
```

- A visdom problem occurs, for example:

```python
ConnectionError: HTTPConnectionPool(host='localhost', port=8097): Max retries exceeded with url: /events (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f4988150990>: Failed to establish a new connection: [Errno 111] Connection refused',))
```

Solution: Comment out all visdom-related code in train.py.

```python
#import visdom

#   vis = visdom.Visdom()

                
                if np.mod(index, 15) == 0:
                    print(r'Testing... Open http://localhost:8097/ to see test result.')
                    # vis.close()
                    '''
                    vis.images(output_np[:, None, :, :], win='test_pred', opts=dict(title='test prediction')) 
                    vis.images(bag_msk_np[:, None, :, :], win='test_label', opts=dict(title='label'))
                    vis.line(all_test_iter_loss, win='test_iter_loss', opts=dict(title='test iter loss'))
                    
                    '''
```

By default, the training script trains 100 epochs for a model. You can change the number of epochs as required.

```python
if __name__ == "__main__":

    train(epo_num=100, show_vgg_params=False)
```



(3) Copy the Caffe and convert folders and the pytorch2caffe-fcn.py and test-fcn.py files to the same directory.

## Conversion

```python
python pytorch2caffe-fcn.py
```

After this command is executed, files such as fcn.caffemodel are generated.

## Result Verification

```python
python test-fcn.py
```
