[EN](README.en.md)|CN
将pytorch框架下的yolov2模型转换成caffe模型

[TOC]

## 准备工作

(1) 下载[yolov2源码](https://github.com/nhviet1009/Yolo-v2-pytorch)

(2)  根据yolov2源码的README，下载训练好的yolov2模型，并放置在trained_models目录下
模型下载地址为：[YOLO trained models](https://drive.google.com/open?id=1Ee6FHQTGuJpNRYSa8DtHWzu4yWNyc7sp)

如果模型无法下载，则需要训练一个yolov2模型放置在trained_models目录下

(3) 拷贝工具中的Caffe、convert文件夹及pytorch2caffe-yolov2.py、test-yolov2.py文件到同级目录

## 开始转换

```python
python pytorch2caffe-yolov2.py
```

执行该命令会生成yolov2.caffemodel等文件

**模型转换FAQ**

- 文件包含错误，如下所示：

```python
Traceback (most recent call last):
  File "pytorch2caffe-yolov2.py", line 10, in <module>
    from src.utils import *
ImportError: No module named src.utils
```

解决方式：在src目录下，添加__init__.py文件即可，__init__.py可以为空，添加完成后src目录结构如下所示。

└─src       		
        ├─__init__.py
        ├─coco_dataset.py
        ├─data_augmentation.py
        ├─loss.py
        ├─pallete
        ├─utils.py
        ├─voc_dataset.py
        └─yolo_net.py

- pytorch属性错误，如下所示：

```python
AttributeError: 'Conv2d' object has no attribute 'padding_mode'
```

解决方式：需要修改torch中conv.py的源代码。

在torch/nn/modules/con.py模块下331行添加`self.padding_mode = 'none'`

在完成yolov2模型转换及结果校验之后，将添加代码删除掉，以免产生影响。

修改前：

```python
    def forward(self, input):
        if self.padding_mode == 'circular':
```

修改后：

```python
    def forward(self, input):
        self.padding_mode = 'none'
        if self.padding_mode == 'circular':
```

## 结果校验

```python
python test_yolov2.py
```

## yolov2的caffe2D转换

在pytorch模型中是通过view与permute算子的组合实现的yolov2中的reorg操作的，在D平台中permute不支持6维处理，可以手动将转换后yolov2.prototxt文件中的view1层、permute1层及view2层替换reorg1层，并将cat1层的输入中的view2，改成reorg1

修改前：

```python
layer {
  name: "view1"
  type: "Reshape"
  bottom: "leaky_relu21"
  top: "view1"
  reshape_param {
    shape {
      dim: 1
      dim: 16
      dim: 28
      dim: 2
      dim: 28
      dim: 2
    }
  }
}
layer {
  name: "permute1"
  type: "Permute"
  bottom: "view1"
  top: "permute1"
  permute_param {
    order: 0
    order: 3
    order: 5
    order: 1
    order: 2
    order: 4
  }
}
layer {
  name: "view2"
  type: "Reshape"
  bottom: "permute1"
  top: "view2"
  reshape_param {
    shape {
      dim: 1
      dim: -1
      dim: 14
      dim: 14
    }
  }
}
layer {
  name: "cat1"
  type: "Concat"
  bottom: "leaky_relu20"
  bottom: "view2"
  top: "cat1"
  concat_param {
    axis: 1
  }
}
```

修改后:

```python
layer {
  name: "reorg1"
  type: "Reorg"
  bottom:"leaky_relu21"
  top: "reorg1"
  reorg_param {
    stride: 2
  }
}

layer {
  name: "cat1"
  type: "Concat"
  bottom: "leaky_relu20"
  bottom: "reorg1"
  top: "cat1"
  concat_param {
    axis: 1
  }
}
```

