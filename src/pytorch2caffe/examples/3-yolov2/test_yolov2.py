import numpy as np
import torch
from src.yolo_net import Yolo
import caffe
from torch.autograd import Variable

input_size = [1, 3, 448, 448]
image = np.random.randint(0, 255, input_size)
input_data = image.astype(np.float32)

model = Yolo(80)
model = torch.load('./trained_models/whole_model_trained_yolo_coco',
                   map_location=lambda storage, loc: storage)
model.eval()
input_var = Variable(torch.from_numpy(input_data))
pytorch_output = model(input_var)

caffe_model = './yolov2.prototxt'
caffe_weights = './yolov2.caffemodel'
caffe_net = caffe.Net(caffe_model, caffe_weights, caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()

diff_conv23 = np.abs(pytorch_output.data.cpu().numpy().flatten()
                     - caffe_out['conv23'].flatten())

print('diff:')
print('diff_max:', diff_conv23.max(), '    ', 'diff_mean:', diff_conv23.mean(), '    ',
      'diff_min:', diff_conv23.min(), '    ', 'diff_median:', np.median(diff_conv23))
