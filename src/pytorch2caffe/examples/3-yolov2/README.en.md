EN|[CN](README.md)
Converting the YOLOv2 Model in PyTorch to a Caffe Model

[TOC]

## Preparations

(1) Download [YOLOv2 source code](https://github.com/nhviet1009/Yolo-v2-pytorch).

(2) Download the trained YOLOv2 model based on the YOLOv2 source code README and place it in the trained_models directory. 

Download the model from [YOLO trained models](https://drive.google.com/open?id=1Ee6FHQTGuJpNRYSa8DtHWzu4yWNyc7sp).

If the model cannot be downloaded, train a YOLOv2 model and place it in the trained_models directory.

(3) Copy the Caffe and convert folders and the pytorch2caffe-yolov2.py and test-yolov2.py files to the same directory.

## Conversion

```python
python pytorch2caffe-yolov2.py
```

After this command is executed, files such as yolov2.caffemodel are generated.

**Model Conversion FAQs**

- The file contains an error, as shown in the following:

```python
Traceback (most recent call last):
  File "pytorch2caffe-yolov2.py", line 10, in <module>
    from src.utils import *
ImportError: No module named src.utils
```

Solution: Add the __init__.py file to the src directory. The __init__.py file content can be empty. After the file is added, the structure of the src directory is as follows:

└─src       		
        ├─__init__.py
        ├─coco_dataset.py
        ├─data_augmentation.py
        ├─loss.py
        ├─pallete
        ├─utils.py
        ├─voc_dataset.py
        └─yolo_net.py

- The PyTorch attribute is incorrect, as shown in the following:

```python
AttributeError: 'Conv2d' object has no attribute 'padding_mode'
```

Solution: Modify the source code of conv.py in the torch file.

Add `self.padding_mode = 'none'` to line 331 in the torch/nn/modules/con.py module.

After the YOLOv2 model is converted and the result is verified, delete the added code to avoid any impact.

Before modification:

```python
    def forward(self, input):
        if self.padding_mode == 'circular':
```

After modification:

```python
    def forward(self, input):
        self.padding_mode = 'none'
        if self.padding_mode == 'circular':
```

## Result Verification

```python
python test_yolov2.py
```

## YOLOv2 to Caffe2D Conversion

In the PyTorch model, the reorg operation in YOLOv2 is implemented by combining the view and permute operators. On the D platform, permute does not support 6-dimensional processing, you can manually replace the view1, permute1, and view2 layers in the yolov2.prototxt file with the reorg1 layer after conversion, and replace view2 in the input of the cat1 layer to reorg1.

Before modification:

```python
layer {
  name: "view1"
  type: "Reshape"
  bottom: "leaky_relu21"
  top: "view1"
  reshape_param {
    shape {
      dim: 1
      dim: 16
      dim: 28
      dim: 2
      dim: 28
      dim: 2
    }
  }
}
layer {
  name: "permute1"
  type: "Permute"
  bottom: "view1"
  top: "permute1"
  permute_param {
    order: 0
    order: 3
    order: 5
    order: 1
    order: 2
    order: 4
  }
}
layer {
  name: "view2"
  type: "Reshape"
  bottom: "permute1"
  top: "view2"
  reshape_param {
    shape {
      dim: 1
      dim: -1
      dim: 14
      dim: 14
    }
  }
}
layer {
  name: "cat1"
  type: "Concat"
  bottom: "leaky_relu20"
  bottom: "view2"
  top: "cat1"
  concat_param {
    axis: 1
  }
}
```

After modification:

```python
layer {
  name: "reorg1"
  type: "Reorg"
  bottom:"leaky_relu21"
  top: "reorg1"
  reorg_param {
    stride: 2
  }
}

layer {
  name: "cat1"
  type: "Concat"
  bottom: "leaky_relu20"
  bottom: "reorg1"
  top: "cat1"
  concat_param {
    axis: 1
  }
}
```

