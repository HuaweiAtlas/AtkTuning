import torch
from src.yolo_net import Yolo
from convert import pytorch_to_caffe
from torch.autograd import Variable

model = Yolo(80)
model = torch.load('./trained_models/whole_model_trained_yolo_coco',
                   map_location=lambda storage, loc: storage)
model.eval()
input_var = Variable(torch.rand([1, 3, 448, 448]))
print('**************start pytorch2caffe***************')
pytorch_to_caffe.trans_net(model, [input_var], 'yolov2')
pytorch_to_caffe.save_prototxt('./yolov2.prototxt')
pytorch_to_caffe.save_caffemodel('./yolov2.caffemodel')
print('**********************done*********************')
