import torch
import math
from Caffe import caffe_net
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn.modules.utils import _pair
import numpy as np

"""
How to support a new layer type:
 layer_name=log.add_layer(layer_type_name)
 top_blobs=log.add_blobs(<output of that layer>)
 layer=caffe_net.Layer_param(xxx)
 <set layer parameters>
 [<layer.add_data(*datas)>]
 log.cnet.add_layer(layer)
"""

# TODO: support the inplace output of the layers


NET_INITTED = False


class TransLog(object):
    def __init__(self):
        """
        doing init() with inputs Variable before using it
        """
        self.layers = {}
        self.detail_layers = {}
        self.detail_blobs = {}
        self._blobs = {}
        self._blobs_data = []
        self.cnet = caffe_net.Caffemodel('')
        self.debug = False

    def init(self, inputs):
        """
        :param inputs: is a list of input variables
        """
        self.add_blobs(inputs)

    def add_layer(self, name='layer'):
        """
        add layer
        :param name: layer name
        :return: a layer
        """
        if name in self.layers:
            return self.layers[name]
        if name not in self.detail_layers.keys():
            self.detail_layers[name] = 0
        self.detail_layers[name] += 1
        name = '{}{}'.format(name, self.detail_layers[name])
        self.layers[name] = name
        if self.debug:
            print("{} was added to layers".format(self.layers[name]))
        return self.layers[name]

    def add_blobs(self, blobs, name='blob', with_num=True):
        """
        add blobs
        :param blobs: blobs
        :param name: blobs name
        :param with_num: have blobs num or not
        :return: result of add blobs
        """
        rst = []
        for blob in blobs:
            self._blobs_data.append(blob)  # to block the memory address be rewrited
            blob = int(id(blob))
            if name not in self.detail_blobs.keys():
                self.detail_blobs[name] = 0
            self.detail_blobs[name] += 1
            if with_num:
                rst.append('{}{}'.format(name, self.detail_blobs[name]))
            else:
                rst.append('{}'.format(name))
            if self.debug:
                print("{}:{} was added to blobs".format(blob, rst[-1]))
            self._blobs[blob] = rst[-1]
        return rst

    def blobs(self, var):
        """
        blobs
        :param var: a set of variable
        :return: blobs of variable
        """
        var = id(var)
        if self.debug:
            print("{}:{} getting".format(var, self._blobs[var]))
        try:
            return self._blobs[var]
        except:
            print("WARNING: CANNOT FOUND blob {}".format(var))
            return None

    def reuse_blob(self, old_tensor, new_tensor):
        """
        set blob to new tensor
        :param old_tensor: old tensor
        :param new_tensor: new tensor
        :return: none
        """
        blob_name = self._blobs[id(old_tensor)]
        self._blobs[id(new_tensor)] = blob_name


log = TransLog()


def _conv2d(raw, input, weight, bias=None, stride=1, padding=0, dilation=1, groups=1):
    """
    2 D convolution operator of torch to caffe
    :param raw: torch operator
    :param input: operator input
    :param weight: operator weight
    :param bias: operator bias
    :param stride: stride value
    :param padding: padding value
    :param dilation: dilation value
    :param groups: groups value
    :return: the result of torch operator
    """
    x = raw(input, weight, bias, stride, padding, dilation, groups)
    name = log.add_layer(name='conv')
    log.add_blobs([x], name='conv')
    layer = caffe_net.Layer_param(name=name, type='Convolution',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    layer.conv_param(x.size()[1], weight.size()[2:], stride=_pair(stride),
                     pad=_pair(padding), dilation=_pair(dilation), bias_term=bias is not None, groups=groups)
    if bias is not None:
        layer.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
    else:
        layer.param.convolution_param.bias_term = False
        layer.add_data(weight.cpu().data.numpy())
    log.cnet.add_layer(layer)
    return x


def _conv_transpose2d(raw, input, weight, bias=None, stride=1, padding=0, output_padding=0, groups=1, dilation=1):
    """
    2 D transpose convolution operator of torch to caffe
    :param raw: torch operator
    :param input: operator input
    :param weight: operator weight
    :param bias: operator weight
    :param stride: stride value
    :param padding: padding value
    :param output_padding: output padding value
    :param groups: groups value
    :param dilation: value
    :return: the result of torch operator
    """
    x = raw(input, weight, bias, stride, padding, output_padding, groups, dilation)
    name = log.add_layer(name='conv_transpose2d')
    log.add_blobs([x], name='conv_transpose2d')
    layer = caffe_net.Layer_param(name=name, type='Deconvolution',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    layer.conv_param(x.size()[1], weight.size()[2:], stride=_pair(stride),
                     pad=_pair(padding), dilation=_pair(dilation), bias_term=bias is not None, groups=groups)
    if bias is not None:
        layer.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
    else:
        layer.param.convolution_param.bias_term = False
        layer.add_data(weight.cpu().data.numpy())
    log.cnet.add_layer(layer)
    return x


def _linear(raw, input, weight, bias=None):
    """
    linear connection operator of torch to caffe
    :param raw: torch operator
    :param input: operator input
    :param weight: operator weight
    :param bias: operator bias
    :return: the result of torch operator
    """
    x = raw(input, weight, bias)
    layer_name = log.add_layer(name='fc')
    top_blobs = log.add_blobs([x], name='fc')
    layer = caffe_net.Layer_param(name=layer_name, type='InnerProduct',
                                  bottom=[log.blobs(input)], top=top_blobs)
    layer.fc_param(x.size()[1])
    if bias is not None:
        layer.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
    else:
        layer.add_data(weight.cpu().data.numpy())
    log.cnet.add_layer(layer)
    return x


def _split(raw, tensor, split_size, dim=0):
    """
    split operator of torch to caffe
    :param raw: torch operator
    :param tensor: tensor
    :param split_size: split size
    :param dim: dimensionality
    :return: the result of torch operator
    """
    # split in pytorch is slice in caffe
    x = raw(tensor, split_size, dim)
    layer_name = log.add_layer('split')
    top_blobs = log.add_blobs(x, name='split')
    layer = caffe_net.Layer_param(name=layer_name, type='Slice',
                                  bottom=[log.blobs(tensor)], top=top_blobs)
    slice_param = caffe_net.pb.SliceParameter(axis=dim)
    layer.param.slice_param.CopyFrom(slice_param)
    log.cnet.add_layer(layer)
    return x


def _pool(type, raw, input, x, kernel_size, stride, padding, ceil_mode):
    """
    pool operator of torch to caffe
    :param type: operator type
    :param raw: torch operator
    :param input: operator input
    :param x:
    :param kernel_size: kernel size
    :param stride: stride value
    :param padding: padding
    :param ceil_mode: ceil mode
    :return: none
    """
    # TODO dilation,ceil_mode,return indices
    layer_name = log.add_layer(name='{}_pool'.format(type))
    top_blobs = log.add_blobs([x], name='{}_pool'.format(type))
    layer = caffe_net.Layer_param(name=layer_name, type='Pooling',
                                  bottom=[log.blobs(input)], top=top_blobs)
    # TODO w,h different kernel, stride and padding
    # processing ceil mode
    layer.pool_param(kernel_size=kernel_size, stride=kernel_size if stride is None else stride,
                     pad=padding, ceil_mode=ceil_mode, type=type.upper())
    log.cnet.add_layer(layer)
    if ceil_mode is False and stride is not None:
        oheight = (input.size()[2] - _pair(kernel_size)[0] + 2 * _pair(padding)[0]) % (_pair(stride)[0])
        owidth = (input.size()[3] - _pair(kernel_size)[1] + 2 * _pair(padding)[1]) % (_pair(stride)[1])
        if oheight != 0 or owidth != 0:
            caffe_out = raw(input, kernel_size, stride, padding, ceil_mode=False)
            return caffe_out


def _max_pool2d(raw, input, kernel_size, stride=None, padding=0, dilation=1,
                ceil_mode=False, return_indices=False):
    """
    2D max pool of torch to caffe
    :param raw: torch operator
    :param input: input
    :param kernel_size: kernel size
    :param stride: stride
    :param padding: padding
    :param dilation: dilation
    :param ceil_mode: fix ceil mode or not
    :param return_indices: return indices or not
    :return: the result of torch operator
    """
    x = raw(input, kernel_size, stride, padding, dilation, ceil_mode, return_indices)
    _pool('max', raw, input, x, kernel_size, stride, padding, ceil_mode)
    return x


def _avg_pool2d(raw, input, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True):
    """
    2D average pool of torch to caffe
    :param raw: torch operator
    :param input: input
    :param kernel_size: kernel size
    :param stride: stride
    :param padding: padding
    :param ceil_mode: fix ceil mode or not
    :param count_include_pad: count include pad
    :return: the result of torch operator
    """
    x = raw(input, kernel_size, stride, padding, ceil_mode, count_include_pad)
    _pool('ave', raw, input, x, kernel_size, stride, padding, ceil_mode)
    return x


def _adaptive_avg_pool2d(raw, input, out_size):
    """
    2D adaptiv average pool of torch to caffe
    :param raw: torch operator
    :param input: input
    :param out_size: out size
    :return: the result of torch operator
    """
    x = raw(input, out_size)
    input_size = input.size()[-1]
    stride = int(math.floor(input_size / out_size))
    kernel_size = input_size - (out_size - 1) * stride
    padding = 0
    name = log.add_layer(name='ave')
    top_blobs = log.add_blobs([x], name='ave')
    layer = caffe_net.Layer_param(name=name, type='Pooling',
                                  bottom=[log.blobs(input)], top=top_blobs)
    layer.pool_param(kernel_size=kernel_size, stride=stride, pad=padding, type='AVE')
    log.cnet.add_layer(layer)
    return x


def _max(raw, *args):
    """
    max of torch to caffe
    :param raw: torch operator
    :param args: arguments
    :return: the result of torch operator
    """
    x = raw(*args)
    if len(args) == 1:
        # TODO max in one tensor
        assert NotImplementedError
    else:
        bottom_blobs = []
        for arg in args:
            bottom_blobs.append(log.blobs(arg))
        layer_name = log.add_layer(name='max')
        top_blobs = log.add_blobs([x], name='max')
        layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                      bottom=bottom_blobs, top=top_blobs)
        layer.param.eltwise_param.operation = 2
        log.cnet.add_layer(layer)
    return x


def _cat(raw, inputs, dim=0):
    """
    cat of torch to caffe
    :param raw: torch operator
    :param inputs: inputs
    :param dim: dimension
    :return: the result of torch operator
    """
    x = raw(inputs, dim)
    bottom_blobs = []
    for input in inputs:
        bottom_blobs.append(log.blobs(input))
    layer_name = log.add_layer(name='cat')
    top_blobs = log.add_blobs([x], name='cat')
    layer = caffe_net.Layer_param(name=layer_name, type='Concat',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.concat_param.axis = dim
    log.cnet.add_layer(layer)
    return x


def _dropout(raw, input, p=0.5, training=False, inplace=False):
    """
    dropout of torch to caffe
    :param raw: torch operator
    :param input: input
    :param p: dropout probability
    :param training: training
    :param inplace: inplace
    :return: the result of torch operator
    """
    x = raw(input, p, training, inplace)
    bottom_blobs = [log.blobs(input)]
    layer_name = log.add_layer(name='dropout')
    top_blobs = log.add_blobs([x], name=bottom_blobs[0], with_num=False)
    layer = caffe_net.Layer_param(name=layer_name, type='Dropout',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.dropout_param.dropout_ratio = p
    layer.param.include.extend([caffe_net.pb.NetStateRule(phase=0)])  # 1 for test, 0 for train
    log.cnet.add_layer(layer)
    return x


def _threshold(raw, input, threshold, value, inplace=False):
    """
    threshold of torch to caffe
    :param raw: torch operator
    :param input: input
    :param threshold: threshold
    :param value: value
    :param inplace: is inplace or not
    :return: the result of torch operator
    """
    if threshold == 0 and value == 0:
        x = raw(input, threshold, value, inplace)
        bottom_blobs = [log.blobs(input)]
        name = log.add_layer(name='relu')
        log.add_blobs([x], name='relu')
        layer = caffe_net.Layer_param(name=name, type='ReLU',
                                      bottom=bottom_blobs, top=[log.blobs(x)])
        log.cnet.add_layer(layer)
        return x
    if value != 0:
        raise NotImplementedError("value !=0 not implemented in caffe")
    x = raw(input, input, threshold, value, inplace)
    bottom_blobs = [log.blobs(input)]
    layer_name = log.add_layer(name='threshold')
    top_blobs = log.add_blobs([x], name='threshold')
    layer = caffe_net.Layer_param(name=layer_name, type='Threshold',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.threshold_param.threshold = threshold
    log.cnet.add_layer(layer)
    return x


def _prelu(raw, input, weight):
    """
    prelu of torch to caffe
    :param raw: torch operator
    :param input: input
    :param weight: weight
    :return: the result of torch operator
    """
    x = raw(input, weight)
    bottom_blobs = [log.blobs(input)]
    name = log.add_layer(name='prelu')
    log.add_blobs([x], name='prelu')
    layer = caffe_net.Layer_param(name=name, type='PReLU',
                                  bottom=bottom_blobs, top=[log.blobs(x)])
    if weight.size()[0] == 1:
        layer.param.prelu_param.channel_shared = True
        layer.add_data(weight.cpu().data.numpy()[0])
    else:
        layer.add_data(weight.cpu().data.numpy())
    log.cnet.add_layer(layer)
    return x


def _relu(raw, input, inplace=True):
    """
    relu of torch to caffe
    :param raw: torch operator
    :param input: input
    :param inplace: is inplace or not
    :return: the result of torch operator
    """
    inplace = False
    x = raw(input, inplace)
    name = log.add_layer(name='relu')
    log.add_blobs([x], name='relu')
    layer = caffe_net.Layer_param(name=name, type='ReLU',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    log.cnet.add_layer(layer)
    return x


def _leaky_relu(raw, input, negative_slope=0.01, inplace=True):
    """
    leaky relu of torch to caffe
    :param raw: torch operator
    :param input: input
    :param negative_slope: negative slope
    :param inplace: is inplace or not
    :return: the result of torch operator
    """
    inplace = False
    x = raw(input, negative_slope, inplace)
    name = log.add_layer(name='leaky_relu')
    log.add_blobs([x], name='leaky_relu')
    layer = caffe_net.Layer_param(name=name, type='ReLU',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    layer.param.relu_param.negative_slope = negative_slope
    log.cnet.add_layer(layer)
    return x


def _sigmoid(raw, input):
    """
    sigmoid of torch to caffe
    :param raw: torch operator
    :param input: input
    :return: the result of torch operator
    """
    x = raw(input)
    name = log.add_layer(name='sigmoid')
    log.add_blobs([x], name='sigmoid')
    layer = caffe_net.Layer_param(name=name, type='Sigmoid',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    log.cnet.add_layer(layer)
    return x


def _softmax(raw, input, dim=None, _stacklevel=3):
    """
    softmax of torch to caffe
    :param raw: torch operator
    :param input: input
    :param dim: dimension
    :param _stacklevel:stack level
    :return: the result of torch operator
    """
    x = raw(input, dim=dim)
    if dim is None:
        dim = F._get_softmax_dim('softmax', input.dim(), _stacklevel)
    bottom_blobs = [log.blobs(input)]
    name = log.add_layer(name='softmax')
    log.add_blobs([x], name='softmax')
    layer = caffe_net.Layer_param(name=name, type='Softmax',
                                  bottom=bottom_blobs, top=[log.blobs(x)])
    layer.param.softmax_param.axis = dim
    log.cnet.add_layer(layer)
    return x


def _batch_norm(raw, input, running_mean, running_var, weight=None,
                bias=None, training=False, momentum=0.1, eps=1e-5):
    """
    batch norm of torch to caffe
    :param raw: torch operator
    :param input: input
    :param running_mean: running mean
    :param running_var: running var
    :param weight: weight
    :param bias: bias
    :param training: is training or not
    :param momentum: momentum
    :param eps: eps
    :return: the result of torch operator
    """
    # because the runing_mean and runing_var will be changed after the _batch_norm operation, we first save the parameters
    running_mean_clone = running_mean.clone()
    running_var_clone = running_var.clone()
    x = raw(input, running_mean, running_var, weight, bias, training, momentum, eps)
    bottom_blobs = [log.blobs(input)]
    layer_name1 = log.add_layer(name='batch_norm')
    top_blobs = log.add_blobs([x], name='batch_norm')
    layer1 = caffe_net.Layer_param(name=layer_name1, type='BatchNorm',
                                   bottom=bottom_blobs, top=top_blobs)
    layer1.batch_norm_param(1, eps=eps)
    layer1.add_data(running_mean_clone.cpu().numpy(), running_var_clone.cpu().numpy(), np.array([1.0]))
    log.cnet.add_layer(layer1)
    layer_name2 = log.add_layer(name='bn_scale')
    layer2 = caffe_net.Layer_param(name=layer_name2, type='Scale',
                                   bottom=top_blobs, top=top_blobs)  # top_blobs
    layer2.param.scale_param.bias_term = True
    layer2.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
    log.cnet.add_layer(layer2)
    return x


def _interpolate(raw, input, size=None, scale_factor=None, mode='bilinear', align_corners=None):
    """
    interpolate of torch to caffe
    :param raw: torch operator
    :param input: input
    :param size: size
    :param scale_factor: scale factor
    :param mode: mode
    :param align_corners: align corners
    :return: the result of torch operator
    """
    x = raw(input, size, scale_factor, mode, align_corners)
    name = log.add_layer(name='upsample')
    top_blobs = log.add_blobs([x], name='upsample')
    layer = caffe_net.Layer_param(name=name, type='Interp',
                                  bottom=[log.blobs(input)], top=top_blobs)
    layer.param.interp_param.height = size[0]
    layer.param.interp_param.width = size[1]
    log.cnet.add_layer(layer)
    return x

# ----- for Variable operations --------


def _view(input, *args):
    """
    view of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw_view(input, *args)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='view')
    top_blobs = log.add_blobs([x], name='view')
    layer = caffe_net.Layer_param(name=layer_name, type='Reshape',
                                  bottom=[log.blobs(input)], top=top_blobs)
    dims = list(args)
    layer.param.reshape_param.shape.CopyFrom(caffe_net.pb.BlobShape(dim=dims))
    log.cnet.add_layer(layer)
    return x


def _add(input, *args):
    """
    add of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__add__(input, *args)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='add')
    top_blobs = log.add_blobs([x], name='add')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 1  # sum is 1
    log.cnet.add_layer(layer)
    return x


def _iadd(input, *args):
    """
    iadd of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__iadd__(input, *args)
    if not NET_INITTED:
        return x
    x = x.clone()
    layer_name = log.add_layer(name='add')
    top_blobs = log.add_blobs([x], name='add')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 1  # sum is 1
    log.cnet.add_layer(layer)
    return x


def _sub(input, *args):
    """
    sub of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__sub__(input, *args)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='sub')
    top_blobs = log.add_blobs([x], name='sub')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 1  # sum is 1
    layer.param.eltwise_param.coeff.extend([1., -1.])
    log.cnet.add_layer(layer)
    return x


def _isub(input, *args):
    """
    isub of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__isub__(input, *args)
    if not NET_INITTED:
        return x
    x = x.clone()
    layer_name = log.add_layer(name='sub')
    top_blobs = log.add_blobs([x], name='sub')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 1  # sum is 1
    log.cnet.add_layer(layer)
    return x


def _mul(input, *args):
    """
    mul of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__sub__(input, *args)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='mul')
    top_blobs = log.add_blobs([x], name='mul')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 0  # product is 1
    log.cnet.add_layer(layer)
    return x


def _imul(input, *args):
    """
    imul of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw__isub__(input, *args)
    if not NET_INITTED:
        return x
    x = x.clone()
    layer_name = log.add_layer(name='mul')
    top_blobs = log.add_blobs([x], name='mul')
    layer = caffe_net.Layer_param(name=layer_name, type='Eltwise',
                                  bottom=[log.blobs(input), log.blobs(args[0])], top=top_blobs)
    layer.param.eltwise_param.operation = 0  # product is 1
    layer.param.eltwise_param.coeff.extend([1., -1.])
    log.cnet.add_layer(layer)
    return x


def _mean(input, *args, **kwargs):
    """
    mean of torch to caffe
    :param input: input
    :param args: args
    :param kwargs: kwargs
    :return: the result of torch operator
    """
    x = raw_mean(input, *args, **kwargs)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='mean')
    top_blobs = log.add_blobs([x], name='mean')
    layer = caffe_net.Layer_param(name=layer_name, type='Reduction',
                                  bottom=[log.blobs(input)], top=top_blobs)
    if len(args) == 1:
        dim = args[0]
    elif 'dim' in kwargs:
        dim = kwargs['dim']
    else:
        raise NotImplementedError('mean operation must specify a dim')

    layer.param.reduction_param.operation = 4
    layer.param.reduction_param.axis = dim
    log.cnet.add_layer(layer)
    return x


def _permute(input, *args):
    """
    permute of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw_permute(input, *args)
    if not NET_INITTED:
        return x
    name = log.add_layer(name='permute')
    top_blobs = log.add_blobs([x], name='permute')
    layer = caffe_net.Layer_param(name=name, type='Permute',
                                  bottom=[log.blobs(input)], top=top_blobs)
    orders = list(args)
    layer.param.permute_param.order.extend(orders)
    log.cnet.add_layer(layer)
    return x


def _contiguous(input, *args):
    """
    contiguous of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw_contiguous(input, *args)
    if not NET_INITTED:
        return x
    log.reuse_blob(input, x)
    return x


class Rp(object):
    """
    function replace class that repalce torch operator to caffe operator
    """
    def __init__(self, raw, replace, **kwargs):
        """
        replace the raw function to replace function
        :param raw: torch operator
        :param replace: caffe operator
        :param kwargs:
        """
        self.obj = replace
        self.raw = raw

    def __call__(self, *args, **kwargs):
        """
        call function
        :param args: args
        :param kwargs: kwargs
        :return:
        """
        if not NET_INITTED:
            return self.raw(*args, **kwargs)
        out = self.obj(self.raw, *args, **kwargs)
        return out


# repalce torch operator to caffe operator
F.conv2d = Rp(F.conv2d, _conv2d)
F.conv_transpose2d = Rp(F.conv_transpose2d, _conv_transpose2d)
F.linear = Rp(F.linear, _linear)
F.max_pool2d = Rp(F.max_pool2d, _max_pool2d)
F.avg_pool2d = Rp(F.avg_pool2d, _avg_pool2d)
F.adaptive_avg_pool2d = Rp(F.adaptive_avg_pool2d, _adaptive_avg_pool2d)
F.dropout = Rp(F.dropout, _dropout)
F.threshold = Rp(F.threshold, _threshold)
F.prelu = Rp(F.prelu, _prelu)
F.relu = Rp(F.relu, _relu)
F.leaky_relu = Rp(F.leaky_relu, _leaky_relu)
F.batch_norm = Rp(F.batch_norm, _batch_norm)
F.softmax = Rp(F.softmax, _softmax)
F.interpolate = Rp(F.interpolate, _interpolate)
F.sigmoid = Rp(F.sigmoid, _sigmoid)

torch.split = Rp(torch.split, _split)
torch.max = Rp(torch.max, _max)
torch.cat = Rp(torch.cat, _cat)
torch.sigmoid = Rp(torch.sigmoid, _sigmoid)

# TODO: other types of the view function
try:
    raw_view = Variable.view
    Variable.view = _view
    raw__add__ = Variable.__add__
    Variable.__add__ = _add
    raw__iadd__ = Variable.__iadd__
    Variable.__iadd__ = _iadd
    raw__sub__ = Variable.__sub__
    Variable.__sub__ = _sub
    raw__isub__ = Variable.__isub__
    Variable.__isub__ = _isub
    raw__mul__ = Variable.__mul__
    Variable.__mul__ = _mul
    raw__imul__ = Variable.__imul__
    Variable.__imul__ = _imul
except:
    # for new version 0.4.0
    for t in [torch.Tensor]:
        raw_view = t.view
        t.view = _view
        raw__add__ = t.__add__
        t.__add__ = _add
        raw__iadd__ = t.__iadd__
        t.__iadd__ = _iadd
        raw__sub__ = t.__sub__
        t.__sub__ = _sub
        raw__isub__ = t.__isub__
        t.__isub__ = _isub
        raw__mul__ = t.__mul__
        t.__mul__ = _mul
        raw__imul__ = t.__imul__
        t.__imul__ = _imul
        raw_mean = t.mean
        t.mean = _mean
        raw_permute = t.permute
        t.permute = _permute
        raw_contiguous = t.contiguous
        t.contiguous = _contiguous


def trans_net(net, input_var, name="model_name"):
    """
    user API, convert torch model to caffe model, include 11 models so far.
    :param net: model net
    :param input_var: input
    :param name: netname
    :return: none
    """
    print('Starting Transform, This will take a while')
    print(len(input_var))
    for i in range(len(input_var)):
        log.init([input_var[i]])
        log.cnet.net.name = name
        log.cnet.net.input.extend([log.blobs(input_var[i])])
        log.cnet.net.input_dim.extend(input_var[i].size())
    global NET_INITTED
    NET_INITTED = True
    if len(input_var) == 1:
        net.forward(input_var[0])
    elif len(input_var) == 2:
        net.forward(input_var[0], input_var[1])
    elif len(input_var) == 3:
        net.forward(input_var[0], input_var[1], input_var[2])
    else:
        net.forward(input_var[0], input_var[1], input_var[2], input_var[3])
    NET_INITTED = False
    print('Transform Completed')


def save_prototxt(save_name):
    """
    user API, save to prototxt
    :param save_name:
    :return: none
    """
    log.cnet.save_prototxt(save_name)


def save_caffemodel(save_name):
    """
    user API, save to caffe model
    :param save_name: model name
    :return: none
    """
    log.cnet.save(save_name)
