EN|[CN](README.md)
# pytorch_to_caffe Model Conversion Tool

[TOC]

## Environment Dependency

Dependency of the pytorch_to_caffe model conversion tool:

pytorch-1.1.0 (mask-rcnn is limited to pytorch0.4.0)

Caffe-SSD version: [Address](https://github.com/chuanqi305/ssd)

Some unsupported operators and operations need to be added to Caffe-SSD.

(1) interpolate operator

(2) permute operator

(3) roi_align_pool operator

(4) roi_pooling operator

(5) upsample operator

(6) reorg operator

(7) ceil_mode added to the pooling layer

numpy

opencv



## Directory Structure

### Tool Directory Structure

├─README.md 	       //Tool description

├─Caffe                         //Caffe-related dependency   

├─convert                     //Conversion tool

└─example       			//Conversion example
        ├─1-fpn
        ├─2-fcn
        ├─3-yolov2
        ├─4-yolov3
        ├─5-fasterrcnn
        ├─6-deeplabv3
        ├─7-deeplabv3+
        ├─8-maskrcnn
        ├─9-ssd
        ├─10-mtcnn
        └─11-openpose

### File Structure in the Caffe Directory

├─Caffe                        

​        ├─__init__.py
​        ├─caffe.proto     			//File generated after Caffe compilation
​        ├─caffe_net.py          	 
​        ├─caffe_pb2.py	   	  //File generated after Caffe compilation
​        └─layer_param.py        

(1) The caffe.proto and caffe_pb2.py files are obtained from the compiled Caffe.

```
/xxx/caffe-master/src/caffe/proto/caffe.proto
/xxx/caffe-master/python/caffe/proto/caffe_pb2.py
/xxx/ indicates the Caffe installation path.
```

(2) The caffe_net.py and layer_param.py files are obtained from the following address:

```
https://github.com/xiaoranchenwai/re-id_mgn/tree/master/pytorch2caffe_MGN/Caffe
```

Modify layer_param.py:

- Modify the code to support Deconvolution conversion.

Before modification:

```
  if self.type!='Convolution':
```

After modification:

```
if self.type not in ['Convolution','Deconvolution']:
```

- Add the pool-layer parameter ceil_mode.

Before modification:

```python
def pool_param(self,type='MAX',kernel_size=2,stride=2,pad=None):
    pool_param=pb.PoolingParameter()
    pool_param.pool=pool_param.PoolMethod.Value(type)
    if len(pair_process(kernel_size)) > 1:
        pool_param.kernel_h = kernel_size[0]
        pool_param.kernel_w = kernel_size[1]
    else:
        pool_param.kernel_size=kernel_size
    if len(pair_process(stride)) > 1:
        pool_param.stride_h = stride[0]
        pool_param.stride_w = stride[1]
    else:
        pool_param.stride=stride   
    
    if pad:
        pool_param.pad=pad
    self.param.pooling_param.CopyFrom(pool_param)
```
After modification:

```python
def pool_param(self,type='MAX',kernel_size=2,stride=2,pad=None,ceil_mode=False):
    pool_param=pb.PoolingParameter()
    pool_param.pool=pool_param.PoolMethod.Value(type)
    if len(pair_process(kernel_size)) > 1:
        pool_param.kernel_h = kernel_size[0]
        pool_param.kernel_w = kernel_size[1]
    else:
        pool_param.kernel_size=kernel_size
    if len(pair_process(stride)) > 1:
        pool_param.stride_h = stride[0]
        pool_param.stride_w = stride[1]
    else:
        pool_param.stride=stride   
    
    if pad:
        pool_param.pad=pad
    if not ceil_mode:
        pool_param.ceil_mode=ceil_mode
    self.param.pooling_param.CopyFrom(pool_param)
```




## Tool Description

### Tool Principles

The pytorch_to_caffe conversion tool calls the trans_net function in the pytorch_to_caffe.py module to convert the PyTorch operation to the Caffe operation during the inference of the PyTorch model.

The function interfaces are as follows:

- trans_net(net,input_var,name="model_name")

  - net: PyTorch model.

  - input_var: input of the PyTorch model in a list.

  - name: name of the PyTorch model to be converted.
- save_prototxt(save_name):
  - save_name：the path of the .prototxt file.

- def save_caffemodel(save_name):
  - save_name：the path of the .caffemodel file.


The pytorch_to_caffe conversion involves the following cases:

(1) Converting an operator in the torch.nn.functional function

(2) Converting a torch operation

(3) Converting a Tensor/Variable operation

(4) Converting a PyTorch custom operator



#### Converting the torch.nn.functional Function

The torch.nn.functional function contains the standard operators in the PyTorch. The following shows the conversion process of the standard operators in the PyTorch. The convolution operator is used as an example.

**(1) Add the replacement function corresponding to the convolution operator to the pytorch_to_caffe.py module.**

```python
def _conv2d(raw, input, weight, bias=None, stride=1, padding=0, dilation=1, groups=1):
    """
    2 D convolution operator of torch to caffe
    :param raw: torch operator
    :param input: operator input
    :param weight: operator weight
    :param bias: operator bias
    :param stride: stride value
    :param padding: padding value
    :param dilation: dilation value
    :param groups: groups value
    :return: the result of torch operator
    """
    x = raw(input, weight, bias, stride, padding, dilation, groups)
    name = log.add_layer(name='conv')
    log.add_blobs([x], name='conv')
    layer = caffe_net.Layer_param(name=name, type='Convolution',
                                  bottom=[log.blobs(input)], top=[log.blobs(x)])
    layer.conv_param(x.size()[1], weight.size()[2:], stride=_pair(stride),
                     pad=_pair(padding), dilation=_pair(dilation), 
                     bias_term=bias is not None, groups=groups)
    if bias is not None:
        layer.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
    else:
        layer.param.convolution_param.bias_term = False
        layer.add_data(weight.cpu().data.numpy())
    log.cnet.add_layer(layer)
    return x
```

Code description:

Execute the PyTorch inference process.

```python
 x = raw(input, weight, bias, stride, padding, dilation, groups)
```

raw indicates the PyTorch operation, and weight,bias,stride,padding,dilation,groups indicates the parameters of the convolution operator.

Add the Caffe layer corresponding to the convolution operator in PyTorch.

```python
name = log.add_layer(name='conv')
log.add_blobs([x], name='conv')
layer = caffe_net.Layer_param(name=name, type='Convolution',
                              bottom=[log.blobs(input)], top=[log.blobs(x)])
```

Add the parameters and weights of the Caffe layer.

```python
layer.conv_param(x.size()[1], weight.size()[2:], stride=_pair(stride),
                 pad=_pair(padding), dilation=_pair(dilation), 
                 bias_term=bias is not None, groups=groups)
if bias is not None:
    layer.add_data(weight.cpu().data.numpy(), bias.cpu().data.numpy())
else:
    layer.param.convolution_param.bias_term = False
    layer.add_data(weight.cpu().data.numpy())

```

Connect the new Caffe layer to the Caffe network.

```python
log.cnet.add_layer(layer)
```

The result of the PyTorch operator inference is returned.

```python
return x
```

**(2) Replace the PyTorch operator.**

```python
F.conv2d = Rp(F.conv2d, _conv2d)
```

F.conv2d indicates the PyTorch operator operation, and _conv2d indicates the corresponding replacement function.



#### Converting a Torch Operation

The following describes the conversion process of the torch operation in the PyTorch. The torch.cat operation is used as an example.

**(1) Add the replacement function corresponding to the torch.cat operation to the pytorch_to_caffe.py module.**

```python
def _cat(raw, inputs, dim=0):
    """
    cat of torch to caffe
    :param raw: torch operator
    :param inputs: inputs
    :param dim: dimension
    :return: the result of torch operator
    """
    x = raw(inputs, dim)
    bottom_blobs = []
    for input in inputs:
        bottom_blobs.append(log.blobs(input))
    layer_name = log.add_layer(name='cat')
    top_blobs = log.add_blobs([x], name='cat')
    layer = caffe_net.Layer_param(name=layer_name, type='Concat',
                                  bottom=bottom_blobs, top=top_blobs)
    layer.param.concat_param.axis = dim
    log.cnet.add_layer(layer)
    return x
```

Code description:

Execute the PyTorch inference process.

```python
  x = raw(inputs, dim)
```

Add the Caffe layer corresponding to the torch.cat operation.

```python
bottom_blobs = []
for input in inputs:
    bottom_blobs.append(log.blobs(input))
layer_name = log.add_layer(name='cat')
top_blobs = log.add_blobs([x], name='cat')
layer = caffe_net.Layer_param(name=layer_name, type='Concat',
                              bottom=bottom_blobs, top=top_blobs)
```

Add the parameters of the Caffe layer.

```python
layer.param.concat_param.axis = dim
```

Connect the new Caffe layer to the Caffe network.

```python
log.cnet.add_layer(layer)
```

The result of the PyTorch operator inference is returned.

```python
return x
```

**(2) Replace the PyTorch operation.**

```python
torch.cat = Rp(torch.cat, _cat)
```

torch.cat indicates the PyTorch operation, and _cat indicates the corresponding replacement function.



#### Converting a Tensor/Variable Operation

The following describes the conversion process of a Tensor/Variable operation in PyTorch. The torch.Tensor.view operation is used as an example.

**(1) Add the replacement function corresponding to the view operation to the pytorch_to_caffe.py module.**

```python
def _view(input, *args):
    """
    view of torch to caffe
    :param input: input
    :param args: args
    :return: the result of torch operator
    """
    x = raw_view(input, *args)
    if not NET_INITTED:
        return x
    layer_name = log.add_layer(name='view')
    top_blobs = log.add_blobs([x], name='view')
    layer = caffe_net.Layer_param(name=layer_name, type='Reshape',
                                  bottom=[log.blobs(input)], top=top_blobs)
    dims = list(args)
    layer.param.reshape_param.shape.CopyFrom(caffe_net.pb.BlobShape(dim=dims))
    log.cnet.add_layer(layer)
    return x
```

Code description:

Execute the PyTorch inference process.

```python
 x = raw_view(input, *args)
```

Check whether the operation needs to be converted. If no, the PyTorch inference result is returned.

```python
 if not NET_INITTED:
        return x
```

Add the Caffe layer corresponding to the view operation.

```python
layer_name = log.add_layer(name='view')
top_blobs = log.add_blobs([x], name='view')
layer = caffe_net.Layer_param(name=layer_name, type='Reshape',
                              bottom=[log.blobs(input)], top=top_blobs)
```

Add the parameters of the Caffe layer.

```python
dims = list(args)
dims[0] = 0  # the first dim should be batch_size
layer.param.reshape_param.shape.CopyFrom(caffe_net.pb.BlobShape(dim=dims))
```

Connect the new Caffe layer to the Caffe network.

```python
log.cnet.add_layer(layer)
```

The PyTorch inference result is returned.

```python
return x
```

**(2) Replace the PyTorch operation.**

```python
raw_view = t.view
t.view = _view
```

raw_view and t.view indicate the PyTorch operations, and _view indicates the replacement function.



#### Converting a PyTorch Custom Operator

The following describes the conversion process of a custom operator in PyTorch. The proposal operator in the Faster R-CNN model is used as an example.

**(1) Import the PyTorch custom operator module to the pytorch_to_caffe.py module.**

```python
from model.rpn.proposal_layer import _ProposalLayer
```

**(2) Add the replacement function corresponding to custom operator to the pytorch_to_caffe.py module.**

```python
def _proposal(self,input):
    global NET_INITTED
    NET_INITTED = False
    x = raw_proposal_forward(self,input)
    name = log.add_layer(name='proposal')
    log.add_blobs([x],name='proposal')
    layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=[log.blobs(input[0]),log.blobs(input[1]),
                                          log.blobs(input[2])],top=[log.blobs(x)])
    layer.param.python_param.module = 'rpn.proposal_layer'
    layer.param.python_param.layer = 'ProposalLayer'
    log.cnet.add_layer(layer)
    NET_INITTED = True
    return x
```

Code description:

Set the global variable NET_INITTED to False to avoid replacing the torch operation and Tensor/Variable operation in the custom operator.

```python
global NET_INITTED
NET_INITTED = False
```

Execute the PyTorch inference process.

```python
x = raw_proposal_forward(self,input)
```

Add the Caffe layer corresponding to the custom operator and convert the PyTorch custom operator to the Python custom operator in the Caffe framework.

```python
name = log.add_layer(name='proposal')
log.add_blobs([x],name='proposal')
layer = caffe_net.Layer_param(name=name,type='Python',
                                  bottom=[log.blobs(input[0]),log.blobs(input[1]),
                                          log.blobs(input[2])],top=[log.blobs(x)])
```

Add the parameters of the Caffe layer.

```python
 layer.param.python_param.module = 'rpn.proposal_layer'
 layer.param.python_param.layer = 'ProposalLayer'
```

Connect the new Caffe layer to the Caffe network and set the NET_INITTED variable to True.

```python
log.cnet.add_layer(layer)
NET_INITTED = True
```

The PyTorch inference result is returned.    

```python
return x
```

**(3) Add the replacement function corresponding to custom operator.**

```python
raw_proposal_forward = _ProposalLayer.forward
_ProposalLayer.forward = _proposal
```

_ProposalLayer.forward and raw_proposal_forward indicate the Python custom operators, and _proposal indicates the replacement function.



### Tool Usage Description

#### (1) Copy the Caffe and convert folders in the conversion tool to the model project directory.

For example:

└─2-fcn       			 //PyTorch model project directory
        ├─assets
        ├─bag_data
        ├─bag_data_msk
        ├─Caffe			  //Conversion tool
        ├─checkpoints
        ├─convert 		//Conversion tool
        ├─BagData.py
        ├─FCN.py
        ├─onehot.py
        ├─README.md
        └─train.py

#### (2) Compile model conversion code.

Add the following code to the pytorch_to_caffe.py module in the convert directory:

```python
from torchvision import models
```

#### (3) Compile model conversion code.

Conversion code:

```python
from convert import pytorch_to_caffe 
import torch
from torch.autograd import Variable

net = torch.load('./checkpoints/fcn_model_0.pt')
net.eval()

input_var = Variable(torch.randn(1,3,160,160))

print '****************start pytorch2caffe *******************'
pytorch_to_caffe.trans_net(net,[input_var],'FCN')
pytorch_to_caffe.save_prototxt('./fcn.prototxt')
pytorch_to_caffe.save_caffemodel('./fcn.caffemodel')
print '******************done*********************************'
```

Code description:

Import dependencies.

```python
from convert import pytorch_to_caffe 
import torch
from torch.autograd import Variable
```

Import a trained model and set it to the inference mode.

```python
net = torch.load('./checkpoints/fcn_model_0.pt')
net.eval()
```

Construct a model input.

```python
input_var = Variable(torch.randn(1,3,160,160))
```

Convert the PyTorch model to the Caffe model.

```python
pytorch_to_caffe.trans_net(net,[input_var],'FCN')
```

Save the Caffe model after conversion.

```python
pytorch_to_caffe.save_prototxt('./fcn.prototxt')
pytorch_to_caffe.save_caffemodel('./fcn.caffemodel')
```

#### (4) Verify precision after conversion.

After model conversion, you can perform precision verification and analysis on the new model if necessary. The final output is the maximum, average, minimum, and median data errors between the PyTorch inference result vector and the Caffe inference result vector.

Code verification:

```python
import torch
import caffe
from torch.autograd import Variable
import numpy as np

#input
input_size = [1,3,160,160]
image = np.random.randint(0,255,input_size)
input_data = image.astype(np.float32)

#do inference
net = torch.load('./checkpoints/fcn_model_0.pt')
net.eval()
input_var = Variable(torch.from_numpy(input_data))
pytorch_out = net.forward(input_var)

#do inference
model = './fcn.prototxt'
weights = './fcn.caffemodel'
caffe_net = caffe.Net(model,weights,caffe.TEST)
caffe_net.blobs['blob1'].data[...] = input_data
caffe_out = caffe_net.forward()

#different between pytorch model result and caffe model result 
print '******diff******'
diff_conv14 = np.abs(pytorch_out.data.cpu().numpy().flatten() - caffe_out['conv14'].flatten())
print 'diff:'
print'diff_max:',diff_conv14.max(),'    ','diff_mean:',diff_conv14.mean(),'    ',\
     'diff_min:',diff_conv14.min(),'    ','diff_median:',np.median(diff_conv14)
```

#### (5) FAQs

If an operator that is not supported is found during the conversion from a PyTorch model to a Caffe model, add the conversion of the corresponding operator based on the tool principles.



